import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class AgendaContatosTest{
    
    @Test
    public void adicionarContatoEPesquisar(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "123123123");
        agenda.adicionar("DBC", "555555");
        assertEquals("123123123", agenda.consultar("Marcos"));
    }
    
    @Test
    public void adicionarContatoEPesquisarPorTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "123123123");
        agenda.adicionar("DBC", "555555");
        assertEquals("DBC", agenda.consultarNome("555555"));
    }
    
    @Test
    public void adicionarContatoEGerarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "123123123");
        agenda.adicionar("DBC", "555555");
        String separador = System.lineSeparator();
        String esperado = String.format("Marcos,123123123%sDBC,555555%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
}
