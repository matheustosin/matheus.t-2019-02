public class Dwarf extends Personagem{
    private boolean equipado;
    
    
    public Dwarf(String nome){
        super(nome);
        this.vida = 110.0;
        this.qtdDano = 10.0;
        this.ganharItem(new Item(1,"Escudo"));
    }
   
    public Status dwarfComStatusMorto(){
        if(this.vida <= 0.0){
            this.status = Status.MORTO;
        }
        return this.status;
    }
    
    public boolean podeSofrerDano(){
        return this.vida > 0;
    }

    public double calcularDano(){
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public void equiparEscudo(){
        equipado = true;
    }  
    
    public String imprimirResultado(){
        return "Dwarf";
    }
}
