import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void PerderVida(){
        Dwarf novoDwarf = new Dwarf("Vilão");
        novoDwarf.diminuirVida();
        assertEquals(100.0,novoDwarf.getVida(), DELTA);
    }    
    
    @Test
    public void diminuirVida(){
        Dwarf dwarf = new Dwarf("mulung");
        assertEquals(110.0,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf dwarf = new Dwarf("mulung");
        for(int i = 0 ; i < 11; i++){
            dwarf.diminuirVida();
        }
        assertEquals(0.0,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void testarSeDwarfEstaMorto(){
        Dwarf dwarf = new Dwarf("mulung");
        for(int i = 0 ; i < 11; i++){
            dwarf.diminuirVida();
        }
        assertEquals(Status.MORTO,dwarf.dwarfComStatusMorto());
    }
    
    @Test
    public void nasceComStatus(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO,dwarf.getStatus());
    }
    
    @Test
    public void perdeVidaEDeveMorrer(){
        Dwarf dwarf = new Dwarf("Gimli");
        for(int i = 0 ; i < 12; i++){
            dwarf.diminuirVida();
        }
        dwarf.diminuirVida();
        assertEquals(Status.MORTO,dwarf.getStatus());
        assertEquals(0.0,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void perdeVidaContinuaVivo(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.diminuirVida();
        //assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
    }
    
    @Test
    public void dwarfNascemComStatusRecemCriado(){
        Dwarf novoDwarf = new Dwarf ("Gimli");
        assertEquals(Status.RECEM_CRIADO,novoDwarf.getStatus());
    }
    
    @Test
    public void darfGanhaUmItem(){
        Dwarf novoDwarf = new Dwarf ("Gimli");  
        Inventario inventario = new Inventario(1);
        Item faca = new Item(1,"faca");
        novoDwarf.ganharItem(faca);
        assertEquals(faca,novoDwarf.getInventario().getItens().get(1));
    }
    
    @Test
    public void darfPerdeUmItem(){
        Dwarf novoDwarf = new Dwarf ("Gimli");  
        Item faca = new Item(1, "faca");
        novoDwarf.ganharItem(faca);
        novoDwarf.perderItem(faca);
        assertEquals(1,novoDwarf.getInventario().getItens().size());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf novoDwarf = new Dwarf ("Gimli");
        assertEquals("Escudo",novoDwarf.getInventario().getDescricoesItens());
    }
    
    @Test
    public void dwarfEquipadoETomaMetadeDano(){
        Dwarf novoDwarf = new Dwarf ("Gimli");
        novoDwarf.equiparEscudo();
        novoDwarf.diminuirVida();
        assertEquals(105.0,novoDwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNaoEquipadoEtomaDanoIntegral(){
        Dwarf novoDwarf = new Dwarf ("Gimli");
        novoDwarf.diminuirVida();
        assertEquals(100.0,novoDwarf.getVida(), DELTA);
    }
}

