public class Elfo extends Personagem{
    private int indiceFlecha;
    private static int qtdElfos;
    
    {
        this.inventario = new Inventario(2);
        indiceFlecha = 1;
    }
    
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        this.ganharItem(new Item(1,"Arco"));
        this.ganharItem(new Item(3, "Flecha"));
        Elfo.qtdElfos++;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    public String getDescFlecha(){
        return this.getFlecha().getDescricao();
    }
    
    
    
    public boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0;
    }
    
       
    public void atirarFlechaAnao(Dwarf anao){
        if (podeAtirarFlecha()){
            this.getFlecha().setQuantidade(getQtdFlecha() -1);
            anao.diminuirVida();
            this.aumentarXp();
            diminuirVida();
        }
    }
    
    public String imprimirResultado(){
        return "Elfo";
    }
}