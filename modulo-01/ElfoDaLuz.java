import java.util.*;

public class ElfoDaLuz extends Elfo{
    private int countAtaq;
    private final int QTD_VIDA_GANHA = 10;
    private final ArrayList<String> ITEM_VALIDO = new ArrayList<>(Arrays.asList( "Espada de Galvorn"));
    
    {
        countAtaq = 0;
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
        qtdDano = 21;
        this.ganharItem(new ItemSempreExistente(1, ITEM_VALIDO.get(0)));
    }
    
    private boolean devePerderVida(){
        return countAtaq %2 ==1;
    }
    
    private void ganharVida(){
        vida += QTD_VIDA_GANHA;
    }
    
    public Item getEspada(){
        return this.getInventario().Buscar(ITEM_VALIDO.get(0));
    }
    
    public void perderItem(Item item){
        boolean possoPerder = !ITEM_VALIDO.contains(item.getDescricao());
        if (possoPerder){
            super.perderItem(item);
        }
    }
    
    public void atacarComEspada(Dwarf anao){
        if (getEspada().getQuantidade() > 0){
            countAtaq++;
            anao.diminuirVida();
            aumentarXp();
            if(devePerderVida())
                diminuirVida();
                else
                    ganharVida();
        }
    }
    
    
}
