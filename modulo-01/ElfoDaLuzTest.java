import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void ElfoDaLuzNaoPerdeEspadaEspecial(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Item espada = new Item(1, "Espada de Galvorn");
        feanor.perderItem(espada);
        assertEquals(3, feanor.getInventario().getItens().size());
    }
    
    @Test
    public void ElfoDaLuzPerde21DeVidaEmAtaqueImpar(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf anao = new Dwarf("Gul");
        feanor.atacarComEspada(anao); 
        assertEquals(79.0, feanor.getVida(), DELTA);
        assertEquals(100.0, anao.getVida(), DELTA);
    }
    
    @Test
    public void ElfoDaLuzGanha10DeVidaEmAtaquePar(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.atacarComEspada(new Dwarf("Gul")); 
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(89.0, feanor.getVida(), DELTA);
    }
    }
