import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoNoturnoTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void elfoNoturnoGanha3XpAoAtirarFlecha(){
        Elfo elfoNoturno = new ElfoNoturno("Noturno");
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        assertEquals(3, elfoNoturno.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoPerde15AoAtirarFlecha(){
        Elfo elfoNoturno = new ElfoNoturno("Noturno");
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        assertEquals(85.0, elfoNoturno.getVida(), DELTA);
        //assertEquals(Status.SOFREU_DANO, elfoNoturno.getStatus());
    }
    
    @Test
    public void elfoNoturnoAtira7FlechasEMorre(){
        Elfo elfoNoturno = new ElfoNoturno("Noturno");
        elfoNoturno.getFlecha().setQuantidade(1000);
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        elfoNoturno.atirarFlechaAnao(new Dwarf("Gimli"));
        assertEquals(0.0, elfoNoturno.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
    }
}
