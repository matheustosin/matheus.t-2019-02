import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    @After
    public void tearDown(){
        System.gc();
    }
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf ("Anao");
        novoElfo.atirarFlechaAnao(anao);
        novoElfo.atirarFlechaAnao(anao);
        novoElfo.atirarFlechaAnao(anao);
        assertEquals(3, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
        assertEquals(100.0, novoElfo.getVida(), 1e-9);
        assertEquals(80.0, anao.getVida(), 1e-9);
    }

    @Test
    public void elfosNacemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez(){
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }
    
    @Test
    public void criarDoisElfoIncrementaContadorDuasVezes(){
        new Elfo("Legolas");
        new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }
    
    @Test
    public void naoCriaElfoNaoIncrementaContador(){
        assertEquals(0, Elfo.getQtdElfos());
    }
}


