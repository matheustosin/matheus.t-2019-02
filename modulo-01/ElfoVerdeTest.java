import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test
    public void elfoVerdeGanha2XpPorUmaFlecha(){
        Elfo elfo = new ElfoVerde("Celebron");
        elfo.atirarFlechaAnao(new Dwarf("Balin"));
        //assertEquals(2, elfo.getExperiencia());
        assertEquals(100.0, elfo.getVida(), 1e-9);
    }
    
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida(){
        Elfo elfo = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfo.ganharItem(arcoDeVidro);
        Inventario inventario = elfo.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(3, "Flecha"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida(){
        Elfo elfo = new ElfoVerde("Celebron");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        elfo.ganharItem(arcoDeMadeira);
        Inventario inventario = elfo.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(3, "Flecha"), inventario.obter(1));
        assertNull(inventario.Buscar("Arco de Madeira"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoValida(){
        Elfo elfo = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfo.ganharItem(arcoDeVidro);
        elfo.perderItem(arcoDeVidro);
        Inventario inventario = elfo.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(3, "Flecha"), inventario.obter(1));
        assertNull(inventario.Buscar("Arco de Vidro"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoInvalida(){
        Elfo elfo = new ElfoVerde("Celebron");
        Item arco = new Item(1, "Arco");
        elfo.perderItem(arco);
        Inventario inventario = elfo.getInventario();
        assertEquals(arco, inventario.obter(0));
        assertEquals(new Item(3, "Flecha"), inventario.obter(1));
    }
}
    

