import java.util.*;
public interface EstrategiasDeAtaque{
    
    public ArrayList<Elfo> noturnosPorUltimo(ArrayList<Elfo> atacantes);
    
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes);
    
    public ArrayList<Elfo> terceiraEstrategia(ArrayList<Elfo> atacantes);
}
