import java.util.*;
    
public class ExercitoDeElfo{
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();;
    private final ArrayList<Class> CLASSES_VALIDAS = new ArrayList<>(Arrays.asList(ElfoVerde.class, ElfoNoturno.class));
    private ArrayList<Elfo> elfos = new ArrayList<>();
    
    public void alistar(Elfo elfo){
        boolean podeAlistar = CLASSES_VALIDAS.contains(elfo.getClass());
        if(podeAlistar){
            elfos.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());
            if(elfoDoStatus == null){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        } 
    }
    public ArrayList<Elfo> getElfos(){
        return this.elfos;
    }
    
    public ArrayList<Elfo> buscar(Status status){
        return this.porStatus.get(status);
    }
    
}
    
    
        
    

