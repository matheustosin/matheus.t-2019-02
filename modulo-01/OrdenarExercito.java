import java.util.*;

public class OrdenarExercito extends ExercitoDeElfo implements EstrategiasDeAtaque{

    private ExercitoDeElfo elfo;
    private final ArrayList<Status> STATUS_PERMITIDO = new ArrayList<>(
       Arrays.asList(
       Status.RECEM_CRIADO,
       Status.SOFREU_DANO
       )
    );
    private ArrayList<ExercitoDeElfo> elfosVivos = new ArrayList<>();
    private HashMap<Class, ArrayList<ExercitoDeElfo>> ordenarAtaque = new HashMap<>();
    
    public void verificaStatus(ExercitoDeElfo elfoAlistado){
       boolean podeParticipar = STATUS_PERMITIDO.contains(elfoAlistado.getElfos().getStatus());
       if(podeParticipar){
           elfosVivos.add(elfoAlistado);
           ArrayList<Class> tipoElfo = ordenarAtaque.get(elfoAlistado.getStatus());
           if(tipoElfo == null){
               tipoElfo = new ArrayList<>();
               ordenarAtaque.put(tipoElfo, elfoAlistado);
           }
           tipoElfo.add(elfoAlistado);
       }
    
    }
}