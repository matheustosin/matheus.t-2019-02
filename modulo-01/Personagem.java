public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    protected double qtdDano;
    protected int qtdExperienciaPorAtaque;
    
    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia = 0;
        qtdExperienciaPorAtaque = 1;
    }
    
    protected Personagem(String nome){
        this.nome = nome;
    }
    
    protected String getNome(){
        return this.nome;
    }
    
    protected Inventario getInventario(){
        return this.inventario;
    }
    
    protected void setNome(String nome){
        this.nome = nome;
    }
    
    protected double getVida(){
        return this.vida;
    }
    
    protected Status getStatus(){
        return this.status;
    }
    
    protected int getExperiencia(){
        return this.experiencia;
    }
    
    protected void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
    
    protected void perderItem(Item item){
        this.inventario.remover(item);
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    protected void aumentarXp(){
        this.experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }
    
        
    protected double calcularDano(){
        return this.qtdDano;
    }
    
    protected void diminuirVida(){
        if(this.podeSofrerDano()){
            //comparacao ? verdadeiro : falso;
            this.vida -= this.vida >= this.calcularDano() ? this.calcularDano(): this.vida;
        
            if (this.vida == 0.0){
                this.status = Status.MORTO;
            }else{
                //this.status = Status.SOFREU_DANO;
            }
        }
    }
    
    protected abstract String imprimirResultado();
}
