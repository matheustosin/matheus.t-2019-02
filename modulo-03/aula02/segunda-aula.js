//Exericio 01

{
    //raio,  raio da circunferência
    //tipoCalculo  se for "A" cálcula a área, se for "C" calcula a circunferência
}
function calcularCirculo({raio, tipoCalculo:tipo}){
    return Math.ceil(tipo =="A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);    
}

let circulo = {
    raio: 3,
    tipoCalculo : "A"
}

//console.log(calcularCirculo(circulo));


//Exercicio02

/*function naoBissexto(ano) {
    return (ano % 400 === 0) || ( ano % 4 === 0 && ano % 100 != 0 ) ? false : true;
}*/


let naoBissexto = ano => (ano % 400 === 0) || ( ano % 4 === 0 && ano % 100 != 0 ) ? false : true;

/*const testes = {
    diaAula : "Segundo",
    local : "DBC",
    naoBissexto(ano){
        return (ano % 400 === 0) || ( ano % 4 === 0 && ano % 100 != 0 ) ? false : true;
    }
}*/


//console.log(testes.naoBissexto(2016));
//console.log(verificaNaoBissexto(2016));



//Exercicio03

function somarPares(array){
    let soma = 0;
    for (let i = 0; i < array.length; i++){
        if(i %2 == 0){
            soma += array[i];
        }
    }
    return soma;
}
//console.log(somarPares( [ 1, 56, 4.34, 6, -2 ] ));

//Exercicio04

/*function adicionar(op1){
    return function(op2){
        return op1+op2;
    }
}*/

let adicionar = op1 => op2 => op1 + op2;

//console.log(adicionar(3)(4)); //7
//console.log(adicionar(5642)(8749)); //14391

/* const is_divisivel = (divisor, numero) > !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

/* const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
const is_divisivel3 = divisivelPor(3);
console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12)); 
console.log(is_divisivel3(20));
console.log(is_divisivel3(11));
console.log(is_divisivel3(12)); */ 

//Lista2 - Exercicio01-02-03



let moedas = (function() {
    
    //Tudo que é privado


    function imprimirMoeda({numero, separadorMilhar, separadorDecimal, colocarMoeda, colocarNegativo}) {
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow( 10, precisao )
            return Math.ceil(numero * fator) / fator
        }

        let qtdCasasMilhares = 3
        let stringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero)%1)
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length
        
        let c = 1
        while(parteInteiraString.length > 0){
            if(c  % qtdCasasMilhares == 0){
                stringBuffer.push( `${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
                parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
            }else if(parteInteiraString.length < qtdCasasMilhares){
                stringBuffer.push( parteInteiraString )
                parteInteiraString = ''
            }
            c++
        }
        stringBuffer.push( parteInteiraString )

        let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
        const numeroFormatado = `${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decimalString }`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }
   return{
        imprimirBRL : (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar : '.',
                separadorDecimal : ',',
                colocarMoeda : numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo : numeroFormatado => `-${ numeroFormatado }`,
                }),
        imprimirGBP : (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar : ',',
                separadorDecimal : '.',
                colocarMoeda : numeroFormatado => `£$ ${ numeroFormatado }`,
                colocarNegativo : numeroFormatado => `-${ numeroFormatado }`,
                }),
        imprimirFR : (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar : '.',
                separadorDecimal : ',',
                colocarMoeda : numeroFormatado => `${ numeroFormatado } €`,
                colocarNegativo : numeroFormatado => `-${ numeroFormatado }`,
                }),
    }


})() 


/* console.log(moedas.imprimirBRL(0)) // “R$ 0.00”
console.log(moedas.imprimirBRL(3498.99)) // “R$ 3,498.99”
console.log(moedas.imprimirBRL(-3498.99)) // “-R$ 3,498.99”
console.log(moedas.imprimirBRL(2313477.0135)) // “R$ 2,313,477.02”

console.log(moedas.imprimirGBP(0)) // “£ 0.00”
console.log(moedas.imprimirGBP(3498.99)) // “£ 3,498.99”
console.log(moedas.imprimirGBP(-3498.99)) // “-£ 3,498.99”
console.log(moedas.imprimirGBP(2313477.0135)) // “£ 2,313,477.02”

console.log(moedas.imprimirFR(0)) // “ 0.00€”
console.log(moedas.imprimirFR(3498.99)) // “ 3,498.99€”
console.log(moedas.imprimirFR(-3498.99)) // “- 3,498.99€”
console.log(moedas.imprimirFR(2313477.0135)) // “ 2,313,477.02€” */

function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]

    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }
    
  
    cardapio = [...cardapio, 'pastel de carne','empada de legumes marabijosa']
    /* cardapio.concat( [
    ] ) */
  
    if ( veggie ) {// TODO: remover alimentos com carne (é obrigatório usar splice!)
      let arr = []
      arr = cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1)
      arr = cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1)
    }

    let resultado = cardapio
                            //.filter(alimento => alimento === 'cuca de uva')
                            .map(alimento => alimento.toUpperCase())
    return resultado;
   
}
  
console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

function criarSanduiche(pao, recheio, queijo) {
    console.log(`Seu Sanduíche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)
}

const ingredientes = ['3 queijos', 'Frango', 'Cheddar']
criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

receberValoresIndefinidos([1, 3, 4, 5]);