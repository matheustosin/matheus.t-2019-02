import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
//import Renderizacao from './component/Renderizacao';

import InfoJsFlix from './pages/InfoJsFlix';
import ReactMirror from './pages/ReactMirror';
import Home from './pages/Home';
import Login from './pages/Login';

import { PrivateRoute } from './component/PrivateRoute';

export default class App extends Component {

  render() {
    return (
      <Router>
        <React.Fragment>
          <section>
            <PrivateRoute path="/" exact component={Home} />
            <Route path="/Login" component={Login} />
            <Route path="/Home" component={Home} />
            <Route path="/InfoJsFlix" component={InfoJsFlix} />
            <Route path="/ReactMirror" component={ReactMirror} />
          </section>
        </React.Fragment>
      </Router>
    );
  }
}

//const TesteRota = () => <h2>Testando Rota</h2>
