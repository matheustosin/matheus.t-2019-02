import React from 'react';
import { Link } from 'react-router-dom';

const Links = () => {
  
    
    return (
        <React.Fragment>
            <Link to="/Home">Home</Link>
            <Link to="/InfoJsFlix">JsFlix</Link>
            <Link to="/ReactMirror">ReactMirror</Link>
        </React.Fragment>
    )
}

export default Links;