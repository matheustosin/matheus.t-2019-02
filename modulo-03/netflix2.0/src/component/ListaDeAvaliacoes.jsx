import React, { Component } from 'react';
import ListaDeEpisodios from '../models/ListaEpisodios';


export default class ListaDeAvaliacoes extends Component {
  constructor(props){
    super(props)
    this.episAvaliados = []
  }

  adicionarEpisodioAvaliado( episodio ) {
    this.episAvaliados.push(episodio)
  }

  render() {
    return(
      <React.Fragment>
        <ul>{ this.episAvaliados }</ul>
      </React.Fragment>
    )
    
  }
}