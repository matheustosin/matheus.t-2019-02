import React, { Component } from 'react';
import './mensagem.css';
import PropTypes from 'prop-types';



const MensagemFlash = (props) => {

	const { exibirMensagem, mensagemTipo} = props
	const sucesso = <span  id="sucesso">Nota registrada!</span>
	const falha = <span  id="falha">Informar uma nota válida(entre 1 e 5).</span>

	function definirTempo( valor ){
		setTimeout(() => {
			props.apagarMensagem()
		}, valor || 3000)
	}

	function gerarMensagem() {
		return ( mensagemTipo ? sucesso : falha )
	}
	
	return ( 
		<div>
			<h5>{ exibirMensagem ? gerarMensagem() : '' }
					{ exibirMensagem ? definirTempo() : '' } </h5>
		</div>
	)
}

/* MensagemFlash.PropTypes = {
	exibirMensagem: PropTypes.bool.isRequired,
	 mensagem: PropTypes.string.isRequired,
	cor: PropTypes.oneOf( ['verde', 'vermelho']) 
}

/*MensagemFlash.defaultProps = {
	cor: 'verde'
}  */

export default MensagemFlash