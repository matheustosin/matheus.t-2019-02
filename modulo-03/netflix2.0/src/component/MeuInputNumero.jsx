import React from 'react';

const MeuInputNumero = ( props ) => {

  const { assistido, episodio} = props

  function gerarCampoNota() {
		return (
			<div>
				{
					episodio.assistido &&  (
						<div className="camponota">
              {typeof props.msgSpan!== undefined ?<span>{props.msgSpan}</span>: "" }
							{/* <span id="pergunta">Digite uma nota para o episódio:</span> */}
							<input id="nota" maxLength= '1' type="number" onBlur={ props.registrarNota } placeholder={ props.infoPH } />
						</div>
					)
				}
			</div>
		)
  }
  
  return ( 
    <div>
      { assistido ? gerarCampoNota() : '' }
    </div>
  )
}

export default MeuInputNumero