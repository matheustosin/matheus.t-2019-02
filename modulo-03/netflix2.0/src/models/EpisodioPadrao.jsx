import React from 'react';

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            
            <h2 className="titulo">{ episodio.nome }</h2>
            <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
            <div className="infoEp">
                <h4>Duração: { episodio.duracao } minutos</h4>
                <h5>Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es) assistido</h5>
                <h5>Temp/Ep: { episodio.temporadaEpisodio }</h5>
                <h4> { episodio.notaEpisodio || 'Sem Nota' }</h4>
            </div>
            <div className="botoes">
               <button onClick={ props.sortearNoComp }>Próximo</button>
               <button onClick={ props.marcarNoComp }>Já Assisti!</button>    
            </div>   
            
        </React.Fragment>
    )
}

export default EpisodioPadrao