export default class Episodios {
    constructor( nome, duracao, temporada, ordemEpisodio, thumbUrl ) {
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = this.qtdVezesAssistido || 0
        this.notaEpisodio = this.notaEpisodio || 0
    }

    get duracaoEmMin() {
        return `${ this.duracao } min`
    }


    get temporadaEpisodio() {
        return `${ this.temporada.toString().padStart( 2, '0') } / ${ this.ordemEpisodio.toString().padStart( 2, '0') }`
    }

    avaliar( nota ) {
        if ( nota >= 1 && nota <= 5){
            this.notaEpisodio = parseInt( nota )
        }
    }
}