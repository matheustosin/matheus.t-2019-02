import Series from './Series'
import PropTypes from 'prop-types'

export default class ListaSeries {
    constructor() {
        this.series = [
           { titulo:'Stranger Things', anoEstreia:2016, diretor: ['Matt Duffer','Ross Duffer'], genero:['Suspense','Ficcao Cientifica','Drama'], elenco: ['Winona Ryder', 'David Harbour', 'Finn Wolfhard', 'Millie Bobby Brown', 'Gaten Matarazzo', 'Caleb McLaughlin', 'Natalia Dyer', 'Charlie Heaton', 'Cara Buono', 'Matthew Modine', 'Noah Schnapp'], temporadas: 2, numeroEpisodios: 17, distribuidora: 'Netflix'},
           { titulo:'Game Of Thrones', anoEstreia:2011, diretor:['David Benioff','D. B. Weiss','Carolyn Strauss','Frank Doelger','Bernadette Caulfield','George R. R. Martin'], genero:['Fantasia','Drama'], elenco:['Peter Dinklage','Nikolaj Coster-Waldau','Lena Headey','Emilia Clarke','Kit Harington','Aidan Gillen','Iain Glen ','Sophie Turner','Maisie Williams','Alfie Allen','Isaac Hempstead Wright'], temporadas:7, numeroEpisodios: 67, distribuidora:'HBO'},
           { titulo:'The Walking Dead',anoEstreia:2010,diretor:['Jolly Dale','Caleb Womble','Paul Gadd','Heather Bellson'],genero:['Terror','Suspense','Apocalipse Zumbi'],elenco:['Andrew Lincoln','Jon Bernthal','Sarah Wayne Callies','Laurie Holden','Jeffrey DeMunn','Steven Yeun','Chandler Riggs ','Norman Reedus','Lauren Cohan','Danai Gurira','Michael Rooker ','David Morrissey'], temporadas:9, numeroEpisodios:122, distribuidora:'AMC'},
           { titulo:'Band of Brothers',anoEstreia:20001,diretor:['Steven Spielberg','Tom Hanks','Preston Smith','Erik Jendresen','Stephen E. Ambrose'],genero:['Guerra'],elenco:['Damian Lewis','Donnie Wahlberg','Ron Livingston','Matthew Settle','Neal McDonough'],temporadas:1,numeroEpisodios:10,distribuidora:'HBO'},
           { titulo:'The JS Mirror',anoEstreia:2017,diretor:['Lisandro','Jaime','Edgar'],genero:['Terror','Caos','JavaScript'],elenco:['Daniela Amaral da Rosa','Antônio Affonso Vidal Pereira da Rosa','Gustavo Lodi Vidaletti','Bruno Artêmio Johann Dos Santos','Márlon Silva da Silva','Izabella Balconi de Moura','Diovane Mendes Mattos','Luciano Maciel Figueiró','Igor Ceriotti Zilio','Alexandra Peres','Vitor Emanuel da Silva Rodrigues','Raphael Luiz Lacerda','Guilherme Flores Borges','Ronaldo José Guastalli','Vinícius Marques Pulgatti'],temporadas:1,numeroEpisodios:40,distribuidora:'DBC'},
           { titulo:'10 Days Why',anoEstreia:2010,diretor:['Brendan Eich'],genero:['Caos','JavaScript'],elenco:['Brendan Eich','Bernardo Bosak'],temporadas:10,numeroEpisodios:10,distribuidora:'JS'},
           { titulo:'Mr. Robot',anoEstreia:2018,diretor:['Sam Esmail'],genero:['Drama','Techno Thriller','Psychological Thriller'],elenco:['Rami Malek','Carly Chaikin','Portia Doubleday','Martin Wallström','Christian Slater'],temporadas:3,numeroEpisodios:32,distribuidora:'USA Network'},
           { titulo:'Narcos',anoEstreia:2015,diretor:['Paul Eckstein','Mariano Carranco','Tim King','Lorenzo O Brien'],genero:['Documentario','Crime','Drama'],elenco:['Wagner Moura','Boyd Holbrook','Pedro Pascal','Joann Christie','Mauricie Compte','André Mattos','Roberto Urbina','Diego Cataño','Jorge A. Jiménez','Paulina Gaitán','Paulina Garcia'],temporadas:3,numeroEpisodios:30,distribuidora:null},
           { titulo:'Westworld',anoEstreia:2016,diretor:['Athena Wickham'],genero:['Ficcao Cientifica','Drama','Thriller','Acao','Aventura','Faroeste'],elenco:['Anthony I. Hopkins','Thandie N. Newton','Jeffrey S. Wright','James T. Marsden','Ben I. Barnes','Ingrid N. Bolso Berdal','Clifton T. Collins Jr.','Luke O. Hemsworth'],temporadas:2,numeroEpisodios:20,distribuidora:'HBO'},
           { titulo:'Breaking Bad',anoEstreia:2008,diretor:['Vince Gilligan','Michelle MacLaren','Adam Bernstein','Colin Bucksey','Michael Slovis','Peter Gould'],genero:['Acao','Suspense','Drama','Crime','Humor Negro'],elenco:['Bryan Cranston','Anna Gunn','Aaron Paul','Dean Norris','Betsy Brandt','RJ Mitte'],temporadas:5,numeroEpisodios:62,distribuidora:'AMC'}
        ].map( e => new Series( e.titulo, e.anoEstreia, e.diretor, e.genero, e.elenco, e.temporadas, e.numeroEpisodios, e.distribuidora ) )

    }

    verificaCampoNulo ( i ) {
        if ( this.series[i].titulo === null ||
             this.series[i].anoEstreia === null ||
             this.series[i].diretor === null ||
             this.series[i].genero === null ||
             this.series[i].elenco === null ||
             this.series[i].temporadas === null ||
             this.series[i].numeroEpisodios === null ||
             this.series[i].distribuidora === null ) {
            return true
        }
        return false

    }

    invalidas() {
        /* 
        let invalidar = this.series.map( e => e.anoEstreia > 2019 ? e.titulo : null)
        invalidar = invalidar.filter(serie => serie != null)
        return invalidar */
        let agora = new Date()
        let seriesInvalidas = []
        for( let i = 0; i < this.series.length; i++ ) {
            if ( this.series[i].anoEstreia > agora.getFullYear() || this.verificaCampoNulo( i ) ) {
                seriesInvalidas.push( this.series[i] )
            }    
        }
        return seriesInvalidas
    }

    filtrarPorAno( ano ) {
        let seriesPorAno = []
        for ( let i = 0; i < this.series.length; i++ ) {
            if ( this.series[i].anoEstreia >= ano ) {
                seriesPorAno.push( this.series[i].titulo )
            }
        }
        return seriesPorAno
    }

    procurarPorNome( nome ) {
        for ( let i = 0; i < this.series.length; i++ ) {
            if ( this.series[i].elenco.includes( nome ) ){
                return true
            }
        }
        return false
    }

    mediaDeEpisodios() {
        const qtdDeSeries = this.series.length
        let nroEpisodiosTotal = 0
        for ( let i = 0; i < this.series.length; i++ ) {
            nroEpisodiosTotal += this.series[i].numeroEpisodios
        }
        return nroEpisodiosTotal / qtdDeSeries
    }

    totalSalarios( index ) {
        const qtdDiretores = this.series[index].diretor.length
        const qtdElenco = this.series[index].elenco.length
        return `R$ ${( qtdDiretores * 100 + qtdElenco * 40 )}`
    }

    queroGenero( genero ) {
        //const seriePesquisada = this.series.find( e=> e.genero.includes( genero ) )
        let array = []
        for (let i = 0; i < this.series.length; i++){
            if ( this.series[i].genero.includes( genero ) ){
                array.push( this.series[i].titulo )
            }
        }
        return array;
    }

    queroTitulo( titulo ) {
        let seriePesquisada = this.series.map( e=> e.titulo.includes( titulo ) ? e.titulo : null )
        seriePesquisada = seriePesquisada.filter(serie => serie != null)
        return seriePesquisada
    }

    creditos( i ) {
        let titulo = this.series[i].titulo
        let diretor = this.series[i].diretor.map( diretor => diretor.split(' ').reverse().join(' ') )        
        diretor.sort()
        diretor = diretor.map( diretor => diretor.split(' ').reverse().join(' ') )
        let elenco = this.series[i].elenco.map( elenco => elenco.split(' ').reverse().join(' '))
        elenco.sort()
        elenco = elenco.map( elenco => elenco.split(' ').reverse().join(' '))
        return { titulo, diretor, elenco }        
    }

    hashtagSecreta() {
        let result
        this.series.forEach( function( serie ) {
            let verifica = serie.elenco.map( e => abreviar(e))
            if ( !verifica.includes( false ) ) {
                result = serie.elenco.map( e => {
                    let index = e.indexOf( '.' ) -1
                    return e.substr( index, 1 )
                })
                result.unshift( '#' )
            }
        })
        return result.join('')
    }
}

function abreviar( str ) {
    return str.includes( '. ' )
}

ListaSeries.propTypes = {
    invalidas: PropTypes.array,
    filtrarPorAno: PropTypes.array,
    procurarPorNome: PropTypes.array,
    mediaDeEpisodios: PropTypes.array,
    totalSalarios: PropTypes.array,
    queroGenero: PropTypes.array,
    queroTitulo: PropTypes.array,
    creditos: PropTypes.array,
    hashtagSecreta: PropTypes.array,
    abreviar: PropTypes.array
}



/*creditos( serie ) {
        const serieDesejada = this.series.find( e => e.titulo === serie.titulo )
        let titulo = serie.titulo
        let diretor = serie.diretor.map( diretor => diretor.split(' ').reverse().join(' ') )
        diretor.sort()
        diretor = diretor.map( diretor => diretor.split(' ').reverse().join(' ') )
        let elenco = serie.elenco.map( elenco => elenco.split(' ').reverse.join(' '))
        elenco.sort()
        elenco = elenco.map( elenco => elenco.split(' ').reverse.join(' '))
        return { titulo, diretor, elenco } 
    }*/