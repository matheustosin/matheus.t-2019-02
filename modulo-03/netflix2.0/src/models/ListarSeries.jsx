import React from 'react'

const ListarSeries = props => {
    const { series } = props
    return (
        <React.Fragment>
            <section>
                <div className="container">
                    {series.map((serie, index) => (
                        typeof serie == 'object' ? (
                            <article className="series" key={ index }>
                                <h1> { serie.titulo }</h1>
                                <h4>Temporadas: { serie.temporadas }</h4>
                                <h5> Episódios: { serie.numeroEpisodios } </h5>
                            </article>  
                        ) : (
                            <article className="series" key={ index }>
                                <h1> { serie }</h1>
                            </article>
                        )
                    ))}
                </div>                    
            </section>
        </React.Fragment>
    )
}

export default ListarSeries