import React, { Component } from 'react';
import './Home.css';
import Links from '../component/Link';
export default class Home extends Component {

	render() {
		return (
			<div className="inicio">
				<React.Fragment>
					<section>
						<Links></Links>
					</section>
				</React.Fragment>
			</div>
		);
	}
}
