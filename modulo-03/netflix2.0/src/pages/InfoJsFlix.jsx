import React, { Component } from 'react';
import './JSFlix.css';
import ListaSeries from '../models/ListaSeries';
import ListarSeries from '../models/ListarSeries';
import Links from '../component/Link';
//import Renderizacao from './component/Renderizacao';

export default class InfoJsFlix extends Component {
  constructor(props) {
    super(props)
    this.ListaSeries = new ListaSeries()
    //this.sortear = this.sortear.bind( this )
    this.state = {
      series: this.ListaSeries.series
    }
  }

  seriesInvalidas() {
    const series = this.ListaSeries.invalidas()
    this.setState({
      series
    })
  }

  buscarPorAno = (evt) => {
    const series = this.ListaSeries.filtrarPorAno(evt.target.value)
    this.setState({
      series
    })
  }

  buscarPorNome = (evt) => {
    const series = this.ListaSeries.procurarPorNome(evt.target.value)
    this.setState({
      series
    })
  }

  mediaDeEpisodios = (evt) => {
    const series = this.ListaSeries.mediaDeEpisodios(evt.target.value)
    this.setState({
      series
    })
  }

  salarios = (evt) => {
    const series = this.ListaSeries.totalSalarios(evt.target.value)
    this.setState({
      series
    })
  }

  buscarGenero = (evt) => {
    const series = this.ListaSeries.queroGenero(evt.target.value)
    this.setState({
      series
    })
  }

  buscarPorTitulo = (evt) => {
    const series = this.ListaSeries.queroTitulo(evt.target.value)
    this.setState({
      series
    })
  }

  chamaCreditos = (evt) => {
    const series = this.ListaSeries.creditos(evt.target.value)
    this.setState({
      series
    })
  }

  elencoAbreviado = () => {
    const series = this.ListaSeries.hashtagSecreta()
    this.setState({
      series
    })
  }

  /* this.seriesInvalidas = this.ListaSeries.invalidas()
  this.buscarPorAno = this.ListaSeries.filtrarPorAno( 2010 )
  this.buscarPorNome = this.ListaSeries.procurarPorNome( 'Aaron Paul' )
  this.mediaDeEpisodios = this.ListaSeries.mediaDeEpisodios( this )
  this.salarios = this.ListaSeries.totalSalarios( 1 )
  this.buscarGenero = this.ListaSeries.queroGenero( 'Suspense' )
  this.buscaPorTitulo = this.ListaSeries.queroTitulo( 'The' )
  this.chamaCreditos = this.ListaSeries.creditos( 0 )
  this.elencoAbreviados = this.ListaSeries.hashtagSecreta() */

  render() {
    const { series } = this.state
    return (
      <div className="App">
        <div className="linkjs">
          <Links></Links>
        </div>
        <div className="botoes-js">
          
          <div>
            <input className="button" type="button" value="Mostrar séries inválidas" onClick={this.seriesInvalidas.bind( this )} />
            <input className="button" type="button" value="Mostrar por ano" onClick={this.buscarPorAno} />
            <input className="button" type="button" value="Buscar por artista" onClick={this.buscarPorNome} />
            <input className="button" type="button" value="Média de Episódios" onClick={this.mediaDeEpisodios} />
            <input className="button" type="button" value="Total de salários por série" onClick={this.salarios} />
          </div>
          <div>
            <input className="button" type="button" value="Mostrar séries de um gênero específico" onClick={this.buscarGenero} />
            <input className="button" type="button" value="Séries por título específico" onClick={this.buscarPorTitulo} />
            <input className="button" type="button" value="Créditos" onClick={this.creditos} />
            <input className="button" type="button" value="Hashtag" onClick={this.hashtagSecreta} />
          </div>

        </div>
        <ListarSeries series={ series } />
      </div>
    );
  }
}
