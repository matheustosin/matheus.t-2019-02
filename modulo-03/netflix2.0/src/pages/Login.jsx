import React, { Component } from 'react';
import Axios from 'axios';

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  this.trocarValoresState = this.trocarValoresState.bind( this )
}

  trocarValoresState( e ) {
    const { name, value } = e.target;
    this.setState ( { 
      [name]: value
    } )
  }
  
  logar( e ) {
    e.preventDefault();

    const { email, password } = this.state
    if ( email && password ) {
      //executa a regra de email
      Axios.post('http://localhost:1337/login', {
        email: this.state.email,
        password: this.state.password
      }).then( resp => {
        localStorage.setItem( 'Authorization', resp.data.token )
        this.props.history.push('/')
        }
      )
    }
  }

  render() {
    return (
      
      <React.Fragment>
        <div className="login">
          <h5>Logar</h5>
          <input type="text" name="email" id="email" placeholder="Digite o email" onChange={ this.trocarValoresState }/>
          <input type="password" name="password" id="password" placeholder="Digite o password" onChange={ this.trocarValoresState }/>
          <button type="button" onClick={ this.logar.bind( this ) }>Logar</button>
        </div>
      </React.Fragment>
    )
  }
}