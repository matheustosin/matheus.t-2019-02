import React, { Component } from 'react';
import './ReactMirror.css';
import ListaEpisodios from '../models/ListaEpisodios';
import EpisodioPadrao from '../models/EpisodioPadrao';
import MensagemFlash from '../component/MensagemFlash';
import Links from '../component/Link';
import MeuInputNumero from '../component/MeuInputNumero';
import * as axios from 'axios';
import ListaDeAvaliacoes from '../component/ListaDeAvaliacoes';
//import CompA, { CompB } from './ExemploComponenteBasico';


class App extends Component {
	constructor(props) {
		super(props)
		this.ListaEpisodios = new ListaEpisodios();
		this.ListaDeAvaliacoes = new ListaDeAvaliacoes();
		//this.sortear = this.sortear.bind( this )
		this.state = {
			episodio: this.ListaEpisodios.episodiosAleatorios,
			exibirMensagem: false,
			assistido: false,
			mensagemTipo: true,
		}
	}

	/* componentDidMount() {
		axios.get('https://pokeapi.co/api/v2/pokemon/').then( response => console.log(response) ) 
	} */

	sortear = () => {
		const episodio = this.ListaEpisodios.episodiosAleatorios
		this.setState({
			episodio
		})
	}

	marcarComoAssistido = () => {
		const { episodio } = this.state
		this.ListaEpisodios.marcarComoAssistido(episodio)
		this.setState({
			episodio,
			assistido: true,
		})
	}

	verificaNota(n) {
		return n >= 1 && n <= 5
	}

	registrarNota(evt) {
		const { episodio } = this.state
		episodio.avaliar(evt.target.value)
		if (this.verificaNota(evt.target.value)) {
			let a = this.ListaDeAvaliacoes.adicionarEpisodioAvaliado(episodio)
			console.log(a)
			this.setState({
				mensagemTipo: true
			})
		} else {
			this.setState({
				mensagemTipo: false
			})

		}
		this.setState({
			episodio,
			exibirMensagem: true,
		})
	}

	apagarMensagem() {
		this.setState( {
			exibirMensagem: false
		})
	}

	logout() {
		localStorage.removeItem('Authorization');
	}

	render() {
		const { episodio, exibirMensagem, assistido, mensagemTipo, avaliado } = this.state
		return (
			<div className="reactmirror">
				<div className="headermirror">
					<div className="background">
						<div className="linkjs">
							<Links></Links>
							<button id="deslogar" type="button" onClick={ this.logout.bind( this ) }>Deslogar</button>
						</div>
						<EpisodioPadrao episodio={episodio} sortearNoComp={this.sortear.bind(this)} marcarNoComp={this.marcarComoAssistido.bind(this)} />
						<MeuInputNumero assistido={assistido} episodio={episodio} registrarNota={this.registrarNota.bind(this)} infoPH={"1-5"} msgSpan='Digite uma nota para o episódio:'/>
						<MensagemFlash exibirMensagem={exibirMensagem}  mensagemTipo={mensagemTipo} apagarMensagem={this.apagarMensagem.bind(this)} />
						<ListaDeAvaliacoes avaliado={avaliado} episodio={episodio} />
					</div>
				</div>
			</div>
		);
	}
}

export default App;





/* registrarNota(evt) {
		const { episodio } = this.state
		episodio.avaliar(evt.target.value)
		
		this.setState({
			episodio,
			exibirMensagem: true
		})
		setTimeout(() => {
			this.setState({
				exibirMensagem: false
			})
		}, 5000)
	} */

	/*  gerarCampoNota() {
		 return (
			 <div>
				 {
					 this.state.episodio.assistido && (
						 <MensagemFlash registrar={this.registrarNota.bind(this)} />
					 )
				 }
			 </div>
		 )
	 } */

