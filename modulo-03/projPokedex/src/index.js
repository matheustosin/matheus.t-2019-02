const pokeApi = new PokeApi()
const idInserida = document.getElementById( 'idSearch' )
const botaoBuscar = document.getElementById( 'BuscarButton' )
const botaoRandom = document.getElementById( 'RandomButton' )
let idIgual = 0
const sorteios = []

function renderizacaoPokemon( pokemon ) {
  const id = document.querySelector( '.id' )
  const nome = document.querySelector( '.nome' )
  const tipo = document.querySelector( '.tipo' )
  const altura = document.querySelector( '.altura' )
  const peso = document.querySelector( '.peso' )
  const estatistica = document.querySelector( '.estatistica' )
  const imagem = document.querySelector( '.imagem' )
  const hide = document.querySelector( '.hide-show' )

  imagem.src = pokemon.imagem

  while ( estatistica.hasChildNodes() ) {
    estatistica.removeChild( estatistica.firstChild )
  }

  while ( tipo.hasChildNodes() ) {
    tipo.removeChild( tipo.firstChild )
  }

  id.innerHTML = `#${ pokemon.id }`
  nome.innerHTML = pokemon.nome
  altura.innerHTML = `Altura: ${ pokemon.conversaoAltura( 10 ) } cm`
  peso.innerHTML = `Peso: ${ pokemon.conversaoPeso( 10 ) } Kg`
  pokemon.estatistica.forEach( estatist => {
    const list = document.createElement( 'li' )
    const elem = document.createElement( 'p' )
    elem.textContent = `${ estatist.stat.name } : ${ estatist.base_stat }`
    list.appendChild( elem )
    estatistica.appendChild( list )
  } )
  pokemon.tipo.forEach( tipos => {
    const li = document.createElement( 'li' )
    const p = document.createElement( 'p' )
    p.textContent = tipos.type.name
    li.appendChild( p )
    tipo.appendChild( li )
  } )
  idInserida.value = pokemon.id
  hide.style.display = 'block'
}

async function buscar() {
  const pokemonEspecifico = await pokeApi.buscarEspecifico( 12 )
  pokemonEspecifico.then( pokemon => {
    const poke = new Pokemon( pokemon )
    renderizacaoPokemon( poke )
    idIgual = idInserida.value
  } )
}
buscar()

function eraseError() {
  document.getElementById( 'error' ).innerText = ''
}

function error( texto ) {
  document.getElementById( 'error' ).innerText = texto
  setTimeout( eraseError, 2500 )
}

function randomizar() { // eslint-disable-line no-unused-vars
  const min = Math.ceil( 1 )
  const max = Math.floor( 802 )
  const numeroSorteado = Math.floor( Math.random() * ( max - min ) ) + min
  if ( sorteios.includes( numeroSorteado ) ) {
    error( `Este pokémon já foi pesquisado ${ numeroSorteado }` )
  } else {
    sorteios.push( numeroSorteado )
    const pokemonEspecifico = pokeApi.buscarEspecifico( numeroSorteado )
    pokemonEspecifico.then( pokemon => {
      const poke = new Pokemon( pokemon )
      renderizacaoPokemon( poke )
    } )
  }
}
botaoRandom.addEventListener( 'click', () => randomizar() )

function validarID( id ) {
  if ( id.length < 0 || idInserida.value < 1 || idInserida.value > 803 ) {
    error( 'Deve ser inserido número entre 1 e 806' )
  } else if ( idIgual === idInserida.value ) {
    error( 'Não procure o mesmo pokémon!' )
  } else {
    const pokemonEspecifico = pokeApi.buscarEspecifico( id )
    pokemonEspecifico.then( pokemon => {
      const poke = new Pokemon( pokemon )
      renderizacaoPokemon( poke )
      idIgual = idInserida.value
    } )
  }
}


botaoBuscar.addEventListener( 'click', () => validarID( idInserida.value ) )
