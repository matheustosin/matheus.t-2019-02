class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name
    this.id = obj.id
    this.tipo = obj.types
    this.altura = obj.height
    this.peso = obj.weight
    this.estatistica = obj.stats
    this.imagem = obj.sprites.front_default
  }

  conversaoAltura( multiplicador ) {
    return this.altura * multiplicador
  }

  conversaoPeso( divisor ) {
    return this.peso / divisor
  }
}
