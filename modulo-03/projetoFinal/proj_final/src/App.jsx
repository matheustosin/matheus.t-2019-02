import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './component/PrivateRoute';
import Home from './Pages/Home';
import Login from './Pages/Login'
import Agencias from './Pages/Agencias'
import Clientes from './Pages/Clientes'
import TiposDeContas from './Pages/TiposDeContas'
import ContasDeClientes from './Pages/ContasDeClientes'

import './App.css';

export default class App extends Component {

  render(){
    return (
      <Router>
        <React.Fragment>
          <section>
            <PrivateRoute path="/" exact component={Home}/>

            <Route path="/login" exact component={Login}/>
            <PrivateRoute path="/agencias" component={Agencias}/>
            <PrivateRoute path="/clientes" component={Clientes}/>
            <PrivateRoute path="/tiposdecontas" component={TiposDeContas}/>
            <PrivateRoute path="/contasdeclientes" component={ContasDeClientes}/>
          </section>
        </React.Fragment>
      </Router>
    );
  }
}

