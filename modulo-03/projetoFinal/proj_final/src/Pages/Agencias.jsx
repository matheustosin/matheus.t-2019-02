import React, { Component } from 'react';
import NavBar from '../component/NavBar';
import BuscarApi from '../component/BuscarApi';
import './agencias.css';
import ComponenteDetalhes from '../component/ComponenteDetalhes';
import Busca from '../component/Busca';

export default class Agencias extends Component {
  constructor( props ) {
    super( props )
    this.api = new BuscarApi()
    this.state = {
      listaAgencias: null
    }
  }



  componentWillMount() {
    this.api.requisListaAgencias()
    setTimeout(() => {
        this.setState({
            listaAgencias: this.api.TodasAgencias
        })
    }, 1000);
  }



  buscarAgencia( evt ) {
    const agencia = this.api.TodasAgencias
    const busca = agencia.filter(ag => ag.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
    this.setState({
        listaAgencias: busca
    })
  }

  serDigital(evt) {
    const { listaAgencias } = this.state
    const agen = listaAgencias.find(e => e.id === evt)
    agen.isDigital = !agen.isDigital
    this.setState({
      listaAgencias
    })
  }

  isDigital() {
    const agencias = this.api.TodasAgencias
    const digitais = agencias.filter(e => e.isDigital === true)
    this.setState({
      listaAgencias: digitais
    })
  }

  render() {
    const { listaAgencias } = this.state
    return(
      <React.Fragment>
        <div>
          <NavBar></NavBar>
          <Busca buscar={ this.buscarAgencia.bind(this) }/> 
          <div className="divDigital">
          <button type="button" className="botaoDigital" onClick={this.isDigital.bind(this)}>Digitais</button>          
          </div>
            { listaAgencias ? ( 
                <div className="sectionCard">
                { listaAgencias.map((agencias, index) => {
                  return (
                      <div className="cardInfo" key={agencias.codigo}>
                        <h6>{agencias.nome}</h6>
                        <ComponenteDetalhes obj={agencias} key={index} ></ComponenteDetalhes>

                        <button type="button" className="mostraDetalhes" onClick={() => this.serDigital(agencias.id)}>SerDigital</button>
                      </div>
                    )
                 })}
               </div>
            ): <h2 className="text-center">Carregando</h2> }
         </div>
      </React.Fragment>
    )
  }
}