import React, { Component } from 'react';
import NavBar from '../component/NavBar';
import BuscarApi from '../component/BuscarApi';
import Busca from '../component/Busca';
import ComponenteDetalhes from '../component/ComponenteDetalhes';


export default class Clientes extends Component {
  constructor( props ) {
    super( props )
    this.api = new BuscarApi()
    this.state = {
      listaClientes: null
    }
  }
  
  componentWillMount() {
    this.api.requisListaClientes()
    setTimeout(() => {
      this.setState({
        listaClientes: this.api.TodosClientes
      })
    }, 1000);
  }

 

  buscarCliente( evt ) {
    const cliente = this.api.TodosClientes
    const busca = cliente.filter(cl => cl.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
    this.setState({
        listaClientes: busca
    })
  }
  
  
  
  render() {
    const { listaClientes } = this.state
    return(
      <React.Fragment>
        <div>
          <NavBar></NavBar>
          <Busca buscar={ this.buscarCliente.bind(this) }/> 
          { listaClientes ? (
            <div className="sectionCard">
              { listaClientes.map((clientes, index) => {
                return (
                  <div className="cardInfo" key={index}>
                    <h6>{clientes.nome}</h6>
                      <ComponenteDetalhes obj={clientes} key={index} ></ComponenteDetalhes>
                  </div>
                )
              })}
              </div>
          ): <h2 className="text-center">Carregando</h2> }
        </div>
        
      </React.Fragment>
    )
  }
}