import React, { Component } from 'react';
import NavBar from '../component/NavBar';
import BuscarApi from '../component/BuscarApi';
import Busca from '../component/Busca';
import ComponenteDetalhes from '../component/ComponenteDetalhes';

export default class ContasDeClientes extends Component {
  constructor( props ) {
    super(props)
    this.api = new BuscarApi()
    this.state = {
      listaContasClientes: null,
    }
  }

  componentWillMount() {
    this.api.requisListaContasClientes()
    setTimeout(() => {
      this.setState({
        listaContasClientes: this.api.TodasContasClientes
      })
    }, 1000);
  }



  buscarContadeCliente( evt ) {
    const contaCliente = this.api.TodasContasClientes
    const busca = contaCliente.filter(cc => cc.tipo.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
    this.setState({
        listaContasClientes: busca
    })
  }

  render() {
    const { listaContasClientes } = this.state
    return(
      <React.Fragment>
        <div>
        <NavBar></NavBar>
          <Busca buscar={ this.buscarContadeCliente.bind(this) }/> 
        { listaContasClientes ? (
          <div className="sectionCard">
            { listaContasClientes.map((contasClientes, index) => {
              return (
                <div className="cardInfo" key={index}>
                  <h6> { contasClientes.cliente.nome }</h6>
                    <ComponenteDetalhes obj={contasClientes} key={index}></ComponenteDetalhes>
                </div>
              )
            })}
          </div>
        ): <h2 className="text-center">Carregando</h2> }
        </div>
      </React.Fragment>
    )
  }
}