import React, { Component } from 'react';
import Axios from 'axios';
import NavBar from '../component/NavBar';

export default class Login extends Component {
  constructor( props ) {
    super( props )
    this.state = {
      email: '',
      senha: ''
    }
  this.trocarValoresState = this.trocarValoresState.bind( this )
  }

  trocarValoresState( e ) {
    const { name, value } = e.target;
    this.setState ( {
      [name]: value
    } )
  }

  logar( e ) {
    e.preventDefault();

    const { email, senha } = this.state
    if ( email && senha ) {
      Axios.post('http://localhost:1337/login', {
        email: this.state.email,
        senha: this.state.senha
      }).then( resp => {
        localStorage.setItem( 'Authorization', resp.data.token )
        this.props.history.push('/')
        }
      )
    }
  }

  render() {
    return (
      <React.Fragment>
        <NavBar></NavBar>
        <div className="card">
          <div className="card-body">
            <h5 className="h4 text-center py-4">Logar</h5>
            <input className="form-control mb-4" type="text" name="email" id="email" placeholder="Digite o email" onChange={ this.trocarValoresState }/>
            <input className="form-control mb-4" type="password" name="senha" id="senha" placeholder="Digite sua senha" onChange={ this.trocarValoresState }/>
            <button className="btn btn-dark" type="button" onClick={ this.logar.bind( this )}>Logar</button>
          </div>
        </div>
      </React.Fragment>
    )    
  }
}
