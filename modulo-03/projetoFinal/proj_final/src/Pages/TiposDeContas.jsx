import React, { Component } from 'react';
import NavBar from '../component/NavBar';
import BuscarApi from '../component/BuscarApi';
import Busca from '../component/Busca';
import ComponenteDetalhes from '../component/ComponenteDetalhes';


export default class TiposDeContas extends Component {
  constructor( props ) {
    super( props )
    this.api = new BuscarApi()
    this.state = {
      listaTiposDeContas: null,
    }
  }

  componentWillMount() {
    this.api.requisListaTipoContas()
    setTimeout(() => {
      this.setState({
        listaTiposDeContas: this.api.TodosTiposContas
      })
    }, 1000);
    
  }

 

  buscarTipoDeConta( evt ) {
    const tipos = this.api.TodosTiposContas
    const busca = tipos.filter(tc => tc.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
    this.setState({
        listaTiposDeContas: busca
    })
  }
  
  render() {
    const { listaTiposDeContas } = this.state
    return(
      <React.Fragment>
        <div>
        <NavBar></NavBar>
          <Busca buscar={ this.buscarTipoDeConta.bind(this) }/> 
          { listaTiposDeContas ? (
            <div className="sectionCard">
              { listaTiposDeContas.map((contas, index) => {
                return (
                  <div className="cardInfo" key={index}>
                    <h6>{ contas.nome }</h6>
                      <ComponenteDetalhes obj={contas} key={index}></ComponenteDetalhes>
                  </div>
                )
              })}
            </div>
          ): <h2 className="text-center">Carregando</h2>}
        </div>

      </React.Fragment>
    )
  }
}