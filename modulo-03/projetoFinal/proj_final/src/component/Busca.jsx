import React, { Component } from 'react';

export default class Busca extends Component {
  constructor(props) {
    super(props)
    this.props = props

  }


  render() {
    const { buscar } = this.props
    return (
      <React.Fragment>
        <div className="divBusca">
          <input type="text" className="barraDeBusca" placeholder="Buscar nome/código"onChange={buscar}></input>
        </div>
      </React.Fragment>
    )
  }
}