import * as axios from 'axios';

export default class BuscarApi {
  constructor() {
    this.obj = []
    this.baseUrl = 'http://localhost:1337'
  }

  requisApi( url, id ) {
    url += id ? `${id}` : ''
    axios.get(`${this.baseUrl}/${url}`, {
      headers: {
        authorization: localStorage.getItem('Authorization')
      }
    })
    .then( resp => {
      this.obj = resp.data
    })
    .catch( function ( error ) {
      console.log(error)
    })
  }

  requisListaAgencias() {
    this.requisApi( 'agencias' )
  }

  requisListaClientes() {
    this.requisApi( 'clientes' )
  }

  requisListaTipoContas() {
    this.requisApi( 'tipoContas' )
  }

  requisListaContasClientes() {
    this.requisApi( 'conta/clientes' )
  }

  get TodasAgencias() {
    return this.obj.agencias
  }

  get TodosClientes() {
    return this.obj.clientes
  }

  get TodosTiposContas() {
    return this.obj.tipos;
}

  get TodasContasClientes() {
    return this.obj.cliente_x_conta
  }

  requisAgencia( id ) {
    this.requisApi( 'agencia/', id )
  }


}