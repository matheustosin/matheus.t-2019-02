import React, { Component, Fragment } from 'react'



export default class ComponenteDetalhes extends Component {
  constructor(props){
    super(props)
    this.state = {
      deveExibir: false
    }
  }

  exibirDetalhes() {
    this.setState( {
      deveExibir: !this.state.deveExibir 
    })
  }

  dados(obj) {

    return (
      <Fragment>
        { this.state.deveExibir ?
        (Object.keys(obj).map((key, index) => {
          if (typeof obj[key] === 'object') {
            return (
              <Fragment>
                <p>{`${key}:`}</p>
                <div className=''>
                  {this.dados(obj[key])}
                </div>
              </Fragment>)
          } else {
            return (<p key={index}>{`${key}: ${obj[key]}`}</p>)
          }
        })
        ): ''
        }
      </Fragment>
    )
  }

  render() {
    const { obj } = this.props
    return (
      <Fragment>
        <div className=''>
          {this.dados(obj)}
        <button type="button" className="mostraDetalhes" onClick={ this.exibirDetalhes.bind(this) }>Detalhes</button>

        </div>

      </Fragment>
    )
  }
}
