import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class NavBar extends Component {
  
  logout() {
    localStorage.removeItem('Authorization');
  }

  render() {
    return (
      <React.Fragment>
      <nav className="navbar navbar-expand-lg navbar-dark primary-color morpheus-den-gradient">
        <Link className="navbar-brand" to="/">DBank</Link>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="basicExampleNav">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="nav-link" to="/Login">Login
                <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/Agencias">Agências</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/Clientes">Clientes</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/TiposDeContas">Tipos De Contas</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/ContasDeClientes">Contas De Clientes</Link>
            </li>
          </ul>
        </div>
      
        <form className="form-inline">
          <div className="md-form my-0">
          <button type="button" className="btn #dd2c00  darken-4 text-white btn-rounded waves-effect" onClick={this.logout.bind( this )}>Logout</button>
          </div>
        </form>
      </nav>
    </React.Fragment>
    )
  }
}