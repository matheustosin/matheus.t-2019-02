import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao';
import TesteRenderizacao from './components/TesteRenderizacao'
//import CompA, { CompB } from './ExemploComponenteBasico';


class App extends Component {
  constructor( props ) {
    super( props )
    this.ListaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.ListaEpisodios.episodiosAleatorios,
      exibirMensagem: false
    }
  }

  sortear = () => {
    const episodio = this.ListaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.ListaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
    })
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    episodio.avaliar ( evt.target.value )
    this.setState( {
      episodio,
      exibirMensagem: true
    })
    setTimeout ( () => {
      this.setState( {
        exibirMensagem: false
      } )
    }, 5000)
    
  }

  gerarCampoNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span> Qual a sua nota para esse episódio?</span>
              <input type="number" placeholder="Avaliação do episódio" onBlur={ this.registrarNota.bind( this ) }/>
            </div>
          )
        }
      </div>
    )
  }





  render(){
    const { episodio, exibirMensagem } = this.state
    return (
      <div className="App">
        <div className="App-Header">
          <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear.bind( this) } marcarNoComp={ this.marcarComoAssistido.bind( this ) }/>
          { this.gerarCampoNota() }
          <h5> { exibirMensagem ? 'Nota registrada com sucesso!' : '' } </h5>
        </div>
      </div>
    );
  }
}

export default App;
