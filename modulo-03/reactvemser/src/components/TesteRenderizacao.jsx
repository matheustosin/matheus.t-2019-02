import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TesteRenderizacao extends Component {
    render() {
        const children = this.props.children
        return (
            //this.props.nome,
            children
        )
    }
}

TesteRenderizacao.PropType = {
    children: PropTypes.element.isRequired
}