package br.com.dbccompany.controleCoworking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControleCoworkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControleCoworkingApplication.class, args);
	}

}
