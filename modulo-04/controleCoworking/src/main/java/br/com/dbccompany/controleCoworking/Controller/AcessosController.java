package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Acessos;
import br.com.dbccompany.controleCoworking.Repository.AcessosRepository;
import br.com.dbccompany.controleCoworking.Service.AcessosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/acesso")
public class AcessosController extends AbstractController<Acessos, AcessosRepository, AcessosService> {
}
