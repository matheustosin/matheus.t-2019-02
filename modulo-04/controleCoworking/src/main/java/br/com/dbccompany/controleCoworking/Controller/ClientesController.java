package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Clientes;
import br.com.dbccompany.controleCoworking.Repository.ClientesRepository;
import br.com.dbccompany.controleCoworking.Service.ClientesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/cliente/")
public class ClientesController extends AbstractController<Clientes, ClientesRepository, ClientesService> {
}
