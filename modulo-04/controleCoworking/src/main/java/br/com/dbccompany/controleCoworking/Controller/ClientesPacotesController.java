package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.ClientesPacotes;
import br.com.dbccompany.controleCoworking.Repository.ClientesPacotesRepository;
import br.com.dbccompany.controleCoworking.Service.ClientesPacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/clientepacote/")
public class ClientesPacotesController extends AbstractController<ClientesPacotes, ClientesPacotesRepository, ClientesPacotesService>{
}
