package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Contato;
import br.com.dbccompany.controleCoworking.Repository.ContatoRepository;
import br.com.dbccompany.controleCoworking.Service.ContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/contato/")
public class ContatoController extends AbstractController<Contato, ContatoRepository, ContatoService>{
}
