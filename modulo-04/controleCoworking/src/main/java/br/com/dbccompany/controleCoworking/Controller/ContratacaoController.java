package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Contratacao;
import br.com.dbccompany.controleCoworking.Repository.ContratacaoRepository;
import br.com.dbccompany.controleCoworking.Service.ContratacaoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/contratacao/")
public class ContratacaoController extends AbstractController<Contratacao, ContratacaoRepository, ContratacaoService>{
}
