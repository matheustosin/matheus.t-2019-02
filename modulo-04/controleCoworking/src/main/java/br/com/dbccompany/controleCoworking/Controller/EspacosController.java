package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Espacos;
import br.com.dbccompany.controleCoworking.Repository.EspacosRepository;
import br.com.dbccompany.controleCoworking.Service.EspacosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/espaco/")
public class EspacosController extends AbstractController<Espacos, EspacosRepository, EspacosService>{
}
