package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.EspacosPacotes;
import br.com.dbccompany.controleCoworking.Repository.EspacosPacotesRepository;
import br.com.dbccompany.controleCoworking.Service.EspacosPacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/espacopacote/")
public class EspacosPacotesController extends AbstractController<EspacosPacotes, EspacosPacotesRepository, EspacosPacotesService>{
}
