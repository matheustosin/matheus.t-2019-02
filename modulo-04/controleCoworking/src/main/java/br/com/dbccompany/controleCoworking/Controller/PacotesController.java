package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Pacotes;
import br.com.dbccompany.controleCoworking.Repository.PacotesRepository;
import br.com.dbccompany.controleCoworking.Service.PacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/pacote/")
public class PacotesController extends AbstractController<Pacotes, PacotesRepository, PacotesService>{
}
