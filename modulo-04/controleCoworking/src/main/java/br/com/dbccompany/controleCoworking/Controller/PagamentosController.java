package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Pagamentos;
import br.com.dbccompany.controleCoworking.Repository.PagamentosRepository;
import br.com.dbccompany.controleCoworking.Service.PagamentosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/pagamento/")
public class PagamentosController extends AbstractController<Pagamentos, PagamentosRepository, PagamentosService>{
}
