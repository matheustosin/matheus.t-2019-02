package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.TipoContato;
import br.com.dbccompany.controleCoworking.Repository.TipoContatoRepository;
import br.com.dbccompany.controleCoworking.Service.TipoContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/tipo-contato/")
public class TipoContatoController extends AbstractController<TipoContato, TipoContatoRepository, TipoContatoService>{
}
