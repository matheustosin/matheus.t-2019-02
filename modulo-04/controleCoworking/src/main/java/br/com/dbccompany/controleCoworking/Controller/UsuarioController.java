package br.com.dbccompany.controleCoworking.Controller;

import br.com.dbccompany.controleCoworking.Entity.Usuarios;
import br.com.dbccompany.controleCoworking.Repository.UsuariosRepository;
import br.com.dbccompany.controleCoworking.Service.UsuariosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/usuario")
public class UsuarioController extends AbstractController<Usuarios, UsuariosRepository, UsuariosService>{

}
