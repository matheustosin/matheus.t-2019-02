package br.com.dbccompany.controleCoworking.Entity;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Acessos extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns({
            @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE", referencedColumnName = "id_espaco"),
            @JoinColumn(name = "ID_CLIENTE_SALDO_CLIENTE", referencedColumnName = "id_cliente")
    })
    private SaldoCliente saldoCliente;

    @Column(name = "IS_ENTRADA")
    private boolean isEntrada = false;

    @Column(name = "VENCIMENTO")
    private Date vencimento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }


}
