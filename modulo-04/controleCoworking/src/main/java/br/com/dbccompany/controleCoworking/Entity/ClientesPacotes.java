package br.com.dbccompany.controleCoworking.Entity;

import javax.persistence.*;

@Entity
public class ClientesPacotes extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLI_PAC_SEQ", sequenceName = "CLI_PAC_SEQ")
    @GeneratedValue(generator = "CLI_PAC_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Clientes cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_PACOTE")
    private Pacotes pacote;

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
