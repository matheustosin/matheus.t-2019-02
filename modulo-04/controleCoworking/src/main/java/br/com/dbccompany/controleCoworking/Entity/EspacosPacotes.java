package br.com.dbccompany.controleCoworking.Entity;

import javax.persistence.*;

@Entity
public class EspacosPacotes extends AbstractEntity{

    @Id
    @Column(name = "ID_ESPACO_PACOTE")
    @SequenceGenerator(allocationSize = 1, name = "ESP_PAC_SEQ", sequenceName = "ESP_PAC_SEQ")
    @GeneratedValue(generator = "ESP_PAC_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @Column(name = "PRAZO")
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "espaco")
    private Espacos espaco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_pacote")
    private Pacotes pacote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }
}
