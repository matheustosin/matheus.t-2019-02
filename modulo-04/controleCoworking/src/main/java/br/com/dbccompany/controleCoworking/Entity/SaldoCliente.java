package br.com.dbccompany.controleCoworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoCliente{

    @EmbeddedId
    private SaldoClienteID id;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @Column(name = "VENCIMENTO")
    private Date vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<Acessos> acessos = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    private Espacos espaco;

    public SaldoClienteID getId() {
        return id;
    }

    public void setId(SaldoClienteID id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }
}
