package br.com.dbccompany.controleCoworking.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class SaldoClienteID implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE")
    private Clientes cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_ESPACO")
    private Espacos espaco;

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

}
