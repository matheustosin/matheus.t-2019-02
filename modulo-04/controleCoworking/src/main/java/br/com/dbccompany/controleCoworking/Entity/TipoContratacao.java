package br.com.dbccompany.controleCoworking.Entity;

public enum TipoContratacao {

    Minutos, Horas, Turnos, Diarias, Semanas, Meses
}
