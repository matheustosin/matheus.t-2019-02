package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer> {

    List<ClientesPacotes> findAll();
}
