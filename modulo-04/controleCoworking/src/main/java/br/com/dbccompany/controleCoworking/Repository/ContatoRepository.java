package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.Entity.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    List<Contato> findAll();
}
