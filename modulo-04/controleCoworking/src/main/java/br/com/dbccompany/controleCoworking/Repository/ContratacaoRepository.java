package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {

    List<Contratacao> findAll();
}
