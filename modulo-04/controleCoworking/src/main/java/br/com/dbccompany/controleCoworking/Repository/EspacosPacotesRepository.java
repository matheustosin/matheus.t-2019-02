package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {

    List<EspacosPacotes> findAll();
}
