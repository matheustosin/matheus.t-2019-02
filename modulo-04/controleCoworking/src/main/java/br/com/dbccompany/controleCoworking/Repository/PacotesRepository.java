package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {

    List<Pacotes> findAll();
}
