package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.Entity.SaldoCliente;
import br.com.dbccompany.controleCoworking.Entity.SaldoClienteID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@Repository
public interface SaldoClientesRepository extends CrudRepository<SaldoCliente, Integer> {

    List<SaldoCliente> findAll();

    List<SaldoCliente> findAllById_Espaco(long idEspaco);

    List<SaldoCliente> findAllById_Cliente(long idCliente);

    void deleteById(SaldoClienteID id);

    SaldoCliente findById(SaldoClienteID id);
}
