package br.com.dbccompany.controleCoworking.Service;

import br.com.dbccompany.controleCoworking.Entity.Clientes;
import br.com.dbccompany.controleCoworking.Entity.Contato;
import br.com.dbccompany.controleCoworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService extends AbstractService<ClientesRepository, Clientes >{

    @Override
    public Clientes salvar(Clientes cliente) throws Exception {
        try {
            boolean verificaTipo = false;
            List<Contato> contatos = cliente.getContatos();
            for (Contato contato : contatos) {
                if (!contato.getTipoContato().getNome().contains("Telefone") || !contato.getTipoContato().getNome().contains("Email")) {
                    verificaTipo = true;
                }
            }

            if (verificaTipo) {
                super.salvar(cliente);
            }
        }
        catch (Exception e) {
            System.out.println("Deve hava contato de Telefone ou Email");
        }
        return null;
    }
}
