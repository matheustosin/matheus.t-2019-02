package br.com.dbccompany.controleCoworking.Service;

import br.com.dbccompany.controleCoworking.Entity.Pagamentos;
import br.com.dbccompany.controleCoworking.Repository.PagamentosRepository;
import org.springframework.stereotype.Service;

@Service
public class PagamentosService extends AbstractService<PagamentosRepository, Pagamentos> {
}
