package br.com.dbccompany.controleCoworking.Service;

import br.com.dbccompany.controleCoworking.Entity.SaldoCliente;
import br.com.dbccompany.controleCoworking.Entity.SaldoClienteID;
import br.com.dbccompany.controleCoworking.Repository.ClientesRepository;
import br.com.dbccompany.controleCoworking.Repository.EspacosRepository;
import br.com.dbccompany.controleCoworking.Repository.SaldoClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    SaldoClientesRepository repository;

    @Autowired
    ClientesRepository clienteRepository;

    @Autowired
    EspacosRepository espacoRepository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente){
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void removerPorId(Integer idCliente, Integer idEspaco){
        SaldoClienteID id = new SaldoClienteID();
        id.setCliente(clienteRepository.findById(idCliente).get());
        id.setEspaco(espacoRepository.findById(idEspaco).get());
        repository.deleteById(id);
    }

    public SaldoCliente buscarPorId(Integer idCliente, Integer idEspaco){
        SaldoClienteID id = new SaldoClienteID();
        id.setCliente(clienteRepository.findById(idCliente).get());
        id.setEspaco(espacoRepository.findById(idEspaco).get());
        return repository.findById(id);
    }

    public List<SaldoCliente> listarTodos(){
        return (List<SaldoCliente>) repository.findAll();
    }

    public List<SaldoCliente> listarTodosDoEspaco(Integer idEspaco){
        return repository.findAllById_Espaco(idEspaco);
    }

    public List<SaldoCliente> listarTodosDoCliente(Integer idCliente){
        return repository.findAllById_Cliente(idCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editarPorIds(Integer idCliente, Integer idEspaco, SaldoCliente saldoCliente){
        SaldoClienteID saldoID = new SaldoClienteID();
        saldoID.setCliente(clienteRepository.findById(idCliente).get());
        saldoID.setEspaco(espacoRepository.findById(idEspaco).get());
        saldoCliente.setId(saldoID);
        return repository.save(saldoCliente);
    }
}
