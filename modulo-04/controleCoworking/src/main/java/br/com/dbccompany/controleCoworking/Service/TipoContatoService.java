package br.com.dbccompany.controleCoworking.Service;

import br.com.dbccompany.controleCoworking.Entity.TipoContato;
import br.com.dbccompany.controleCoworking.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends AbstractService<TipoContatoRepository, TipoContato> {
}
