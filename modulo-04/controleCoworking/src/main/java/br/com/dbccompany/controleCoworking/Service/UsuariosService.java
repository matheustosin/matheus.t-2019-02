package br.com.dbccompany.controleCoworking.Service;

import br.com.dbccompany.controleCoworking.Entity.Usuarios;
import br.com.dbccompany.controleCoworking.Repository.UsuariosRepository;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class UsuariosService extends AbstractService<UsuariosRepository, Usuarios> {

    @Override
    public Usuarios salvar(Usuarios usuario) throws Exception {
        if(usuario.getSenha().length() < 6) {
            throw new Exception("senha invalida");
        }
            String password = usuario.getSenha();
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            }
            md.update(password.getBytes(), 0, password.length());
            BigInteger newPassword = new BigInteger(1, md.digest());

            password = String.format("%1$032X", newPassword);
            usuario.setSenha(password);
            return super.salvar(usuario);
    }

}




