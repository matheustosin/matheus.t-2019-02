package br.com.dbccompany.controleCoworking.Repository;


import br.com.dbccompany.controleCoworking.Entity.Clientes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClientesRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClientesRepository cliRep;

    @Test
    public void procurarPaisesPorNome() {
        Clientes cliente = new Clientes();
        cliente.setNome("Legolas");
        entityManager.persist(cliente);
        entityManager.flush();

        Clientes resposta = cliRep.findByName(cliente.getNome());
        assertThat(resposta.getNome()).isEqualTo(cliente.getNome());
    }
}
