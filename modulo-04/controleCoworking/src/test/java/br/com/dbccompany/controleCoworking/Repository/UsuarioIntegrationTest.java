package br.com.dbccompany.controleCoworking.Repository;

import br.com.dbccompany.controleCoworking.ControleCoworkingApplicationTests;
import br.com.dbccompany.controleCoworking.Controller.UsuarioController;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class UsuarioIntegrationTest extends ControleCoworkingApplicationTests{

    private MockMvc mock;

    //@Autowired
    private UsuarioController controller;

    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetApiStatusOk() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/api/usuario")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}
