package br.com.dbccompany.controleCoworking.Repository;


import br.com.dbccompany.controleCoworking.Entity.Clientes;
import br.com.dbccompany.controleCoworking.Entity.Usuarios;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuariosRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuariosRepository usuRep;

    @Test
    public void procurarUsuariosPorNome() {
        Usuarios usuario = new Usuarios();
        usuario.setNome("Legolas");
        entityManager.persist(usuario);
        entityManager.flush();

        Usuarios resposta = usuRep.findByName(usuario.getNome());
        assertThat(resposta.getNome()).isEqualTo(usuario.getNome());
    }
}
