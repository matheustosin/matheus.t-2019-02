import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './Component/PrivateRoute';
import Login from './Pages/Login';
import Home from './Pages/Home';
import Elfos from './Pages/Elfos';

import './App.css';

export default class App extends Component {

  render(){
    return (
      <Router>
        <React.Fragment>
          <section>
          <PrivateRoute path="/" exact component={Home}/>
          <Route path="/login" component={Login}/>
          <Route path="/elfos" component={Elfos}/>
          </section>
        </React.Fragment>
      </Router>
    );
  }
}