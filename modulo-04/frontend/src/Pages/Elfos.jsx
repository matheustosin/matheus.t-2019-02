import React, { Component } from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom'

export default class Elfo extends Component {
	constructor(props) {
		super(props)
		this.state = {
			elfos: []
		}
	}


	
	componentDidMount() {
		const header = {
			headers: { Authorization: localStorage.getItem('Authorization') }
		}
		axios.get('http://localhost:8080/api/elfo/', header).then(resp => {
			const dado = resp.data;
			this.setState({
				elfos: dado
			})
		})
	}

	render() {
		const { elfos } = this.state
		return (
			<React.Fragment>
				<h1>Elfos</h1>
				{elfos.map((dado, index) => {
					return (
						<React.Fragment key={index}>
							<div>
								<div>
									<p>ID: {dado.idPersonagem}</p>
									<p>Nome: {dado.nome}</p>
									<p>Experiencia: {dado.experiencia}</p>
									<p>Vida: {dado.vida}</p>
									<p>Raça: {dado.raca}</p>
									<p>Dano: {dado.dano}</p><br/>
								</div>
							</div>
						</React.Fragment>
					)
				})}
			</React.Fragment>
		)
	}
}