import React, { Component } from 'react';
import Axios from 'axios';
//import NavBar from '../component/NavBar';

export default class Login extends Component {
  constructor( props ) {
    super( props )
  this.trocarValoresState = this.trocarValoresState.bind( this )
  }

  trocarValoresState( e ) {
    const { name, value } = e.target;
    this.setState ( {
      [name]: value
    } )
  }

  logout() {
    localStorage.removeItem('Authorization');
    this.props.history.push('/login')
  }

  render() {
    return (
      <React.Fragment>
        <div className="">
          <div className="">
            <button className="" type="button" onClick={ this.logout.bind( this )}>Logout</button>
          </div>
        </div>
      </React.Fragment>
    )    
  }
}
