import React, { Component } from 'react';
import Axios from 'axios';
//import NavBar from '../component/NavBar';

export default class Login extends Component {
  constructor( props ) {
    super( props )
    this.state = {
      username: '',
      password: ''
    }
  this.trocarValoresState = this.trocarValoresState.bind( this )
  }

  trocarValoresState( e ) {
    const { name, value } = e.target;
    this.setState ( {
      [name]: value
    } )
  }

  

  logar( e ) {
    e.preventDefault();

    const { username, password } = this.state
    if ( username && password ) {
      Axios.post('http://localhost:8080/login', {
        username: this.state.username,
        password: this.state.password
      }).then( resp => {
        localStorage.setItem( 'Authorization', resp.headers.authorization )
        this.props.history.push('/')
        }
      )
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="">
          <div className="">
            <h5 className="">Logar</h5>
            <input className="" type="text" name="username" id="username" placeholder="Digite o username" onChange={ this.trocarValoresState }/>
            <input className="" type="password" name="password" id="password" placeholder="Digite sua password" onChange={ this.trocarValoresState }/>
            <button className="" type="button" onClick={ this.logar.bind( this )}>Logar</button>
          </div>
        </div>
      </React.Fragment>
    )    
  }
}
