import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main (String[] args){

        Connection conn = Connector.connect();
        try {
//            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'")
//                    .executeQuery();
//            if (!rs.next()) {
//                conn.prepareStatement("CREATE TABLE PAISES(\n"
//                    + "ID_PAIS INTEGER NOT NULL PRIMARY KEY, \n"
//                    + "NOME VARCHAR(100) NOT NULL\n"
//                    + " )\n").execute();
//            }

//            PreparedStatement pst = conn.prepareStatement("insert into paises(id_pais, nome) "
//                    +"values(paises_seq.nextval, ?)");
//            pst.setString(1, "Argentina");
//            pst.executeUpdate();

//            rs = conn.prepareStatement("select * from paises").executeQuery();
//            while (rs.next()) {
//                System.out.println(String.format("Nome do País: %s", rs.getString("nome")));
//            }

//            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'")
//                    .executeQuery();
//            if (!rs.next()) {
//                conn.prepareStatement("CREATE TABLE ESTADOS(\n" +
//                        "  ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n" +
//                        "  NOME VARCHAR2(100) NOT NULL,\n" +
//                        "  FK_ID_PAIS INTEGER NOT NULL,\n" +
//                        "  FOREIGN KEY (FK_ID_PAIS)\n" +
//                        "    REFERENCES PAISES(ID_PAIS)\n" +
//                        ")").execute();
//            }

//            PreparedStatement pst = conn.prepareStatement("insert into estados(id_estado, nome, fk_id_pais) "
//                    +"values(estados_seq.nextval, ?, (select id_pais from paises where nome = 'Argentina'))");
//            pst.setString(1, "Buenos Aires");
//            pst.executeUpdate();


            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'")
                    .executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE BAIRROS(\n" +
                        "  ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "  NOME VARCHAR2(100) NOT NULL,\n" +
                        "  FK_ID_CIDADE INTEGER NOT NULL,\n" +
                        "  FOREIGN KEY (FK_ID_CIDADE)\n" +
                        "    REFERENCES CIDADES(ID_CIDADE)\n" +
                        ")").execute();
            }

            PreparedStatement pst = conn.prepareStatement("insert into bairros(id_bairro, nome, fk_id_cidade) "
                    +"values(bairros_seq.nextval, ?, (select id_cidade from cidades where nome = 'Buenos Aires'))");
            pst.setString(1, "Caminito");
            pst.executeUpdate();


        } catch(SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXÃO", ex);

        }
    }
}
