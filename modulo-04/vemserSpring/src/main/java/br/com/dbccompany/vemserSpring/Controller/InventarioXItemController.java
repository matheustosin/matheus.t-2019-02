package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventItem")
public class InventarioXItemController {
    @Autowired
    InventarioXItemService service = new InventarioXItemService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioXItem> todosInventarioXItem() {
        return service.todosInventarioXItem();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioXItem novoInventarioXItem(@RequestBody InventarioXItem inventItem) {
        return service.salvar(inventItem);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioXItem editarInventarioXItem(@PathVariable Integer id, @RequestBody InventarioXItem inventItem) {
        return service.editar(id, inventItem);
    }
}


//http://localhost:8080/api/inventItem postman