package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Item;
import br.com.dbccompany.vemserSpring.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/item")
public class ItemController {
    @Autowired
    ItemService service = new ItemService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<Item> todosItens() {
        return service.todosItens();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Item novoItem(@RequestBody Item item) {
        return service.salvar(item);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Item editarItem(@PathVariable Integer id, @RequestBody Item item) {
        return service.editar(id, item);
    }
}


//http://localhost:8080/api/item postman