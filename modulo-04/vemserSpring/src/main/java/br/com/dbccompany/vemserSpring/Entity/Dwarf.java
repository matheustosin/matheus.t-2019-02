package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Entity;

@Entity
public class Dwarf extends Personagem {

    public Dwarf() {
        super.setRaca(RacaType.DWARF);
    }
}
