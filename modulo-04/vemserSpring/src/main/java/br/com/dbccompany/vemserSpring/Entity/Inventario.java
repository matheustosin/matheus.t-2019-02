package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Inventario {

    @Id
    @Column(name = "ID_INVENTARIO")
    @SequenceGenerator(allocationSize = 1, name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
    @GeneratedValue(generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idInventario;

    @Column(name = "TAMANHO")
    private Integer tamanho;

    @OneToMany(mappedBy = "inventario", cascade = CascadeType.ALL)
    private List<InventarioXItem> inventItem = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "FK_ID_PERSONAGEM")
    private Personagem personagem;

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public List<InventarioXItem> getInventItem() {
        return inventItem;
    }

    public void setInventItem(List<InventarioXItem> inventItem) {
        this.inventItem = inventItem;
    }

    public Personagem getPersonagem() {
        return personagem;
    }

    public void setPersonagem(Personagem personagem) {
        this.personagem = personagem;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    //    @OneToMany(mappedBy = "inventario", cascade = CascadeType.ALL)
//    private List<INVENTARIOS_X_ITENS> InventItem = new ArrayList<>();

//    public List<INVENTARIOS_X_ITENS> getInventItem() {
//        return InventItem;
//    }
//
//    public void setInventItem(List<INVENTARIOS_X_ITENS> inventItem) {
//        InventItem = inventItem;
//    }

    //    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "INVENTARIOS_X_ITENS",
//                    joinColumns = { @JoinColumn(name = "fk_id_inventario")},
//                    inverseJoinColumns = { @JoinColumn(name = "fk_id_item")})
//    private List<Item> itens = new ArrayList<>();

    //    public void adicionar(Item novoItem){
//        itens.add(novoItem);
//    }

//
//    public Item Buscar(String descricao){
//        for ( Item itemAtual : this.itens){
//            boolean encontrei = itemAtual.getDescricao().equals(descricao);
//            if (encontrei){
//                return itemAtual;
//            }
//        }
//        return null;
        /*for(int i = 0; i < this.items.size(); i++){
            if (items.get(i).getDescricao().equals(descricao)){
            return items.get(i);
            }
        }*/
//    }
//
//    public Item obter(int posicao){
//        if (posicao >= this.itens.size()){
//            return null;
//        }
//        return this.itens.get(posicao);
//    }
//
//    public void remover(Item item){
//        this.itens.remove(item);
//    }
//
//    public String getDescricoesItens(){
//        StringBuilder descricoes = new StringBuilder();
//        for(int i = 0; i < itens.size(); i++){
//            Item item = this.itens.get(i);
//            if(itens != null){
//                descricoes.append(item.getDescricao());
//                descricoes.append(",");
//            }
//        }
//
//        return descricoes.length() > 0 ? descricoes.substring(0,(descricoes.length()-1)) : descricoes.toString();
//    }
//
//    public Item getMaiorQuantidade(){
//        int indice = 0, maiorQuantidade = 0;
//        for(int i = 0; i < this.itens.size(); i++){
//            Item item = this.itens.get(i);
//            if(itens != null){
//                if(item.getQuantidade() > maiorQuantidade){
//                    maiorQuantidade = item.getQuantidade();
//                    indice = i;
//                }
//            }
//        }
//
//        return this.itens.size() > 0 ? this.itens.get(indice) : null;
//    }
//
//    public boolean verificaEscudo(){
//        for ( Item itemAtual : this.itens){
//            boolean encontrei = itemAtual.getDescricao().equals("Escudo");
//            if (encontrei){
//                return true;
//            }
//        }
//        return false;
//    }

//    public ArrayList<Item> inverter(){
//        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
//
//        for ( int i = this.itens.size() -1; i>= 0; i--){
//            listaInvertida.add(this.itens.get(i));
//        }
//
//        return listaInvertida;
//    }

//    public void ordenarItens(){
//        this.ordenarItens(TipoOrdenacao.ASC);
//    }

//    public void ordenarItens(TipoOrdenacao ordenacao){
//        for (int i = 0; i < this.itens.size(); i++){
//            for (int j = 0; j < this.itens.size() -1; j++){
//                Item atual = this.itens.get(j);
//                Item proximo = this.itens.get(j);
//                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
//                if(deveTrocar){
//                    Item itemTrocado = atual;
//                    this.itens.set(j, proximo);
//                    this.itens.set(j + 1, itemTrocado);
//                }
//            }
//        }
//    }
}
