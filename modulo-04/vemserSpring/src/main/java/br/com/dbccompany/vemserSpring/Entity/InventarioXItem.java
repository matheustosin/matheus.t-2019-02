package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
public class InventarioXItem {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(allocationSize = 1, name = "INVENTARIO_ITEM_SEQ", sequenceName = "INVENTARIO_ITEM_SEQ")
    @GeneratedValue(generator = "INVENTARIO_ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idInventItem;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "INVENTARIO_ID")
    private Inventario inventario;

    public Integer getIdInventItem() {
        return idInventItem;
    }

    public void setIdInventItem(Integer idInventItem) {
        this.idInventItem = idInventItem;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }
}
