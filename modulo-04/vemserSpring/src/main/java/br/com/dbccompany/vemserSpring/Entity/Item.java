package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Item {

    @Id
    @Column(name = "ID_ITEM")
    @SequenceGenerator(allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
    @GeneratedValue(generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idItem;

    @Column(name = "DESCRICAO")
    private String descricao;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    private List<InventarioXItem> itemInvent = new ArrayList<>();

    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<InventarioXItem> getItemInvent() {
        return itemInvent;
    }

    public void setItemInvent(List<InventarioXItem> itemInvent) {
        this.itemInvent = itemInvent;
    }
}