package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personagem {

    @Id
    @Column(name = "ID_PERSONAGEM")
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idPersonagem;

    @Column(name = "NOME", length = 100)
    private String nome;

    @Enumerated(EnumType.STRING)
    private Status status = Status.RECEM_CRIADO;

    @Column(name = "vida")
    private Double vida;

    @Column(name = "dano")
    private Double dano;

    @Column(name = "XP")
    private Integer experiencia;

    @Enumerated(EnumType.STRING)
    private RacaType raca;

    @OneToOne(mappedBy = "personagem")
    private Inventario inventario;


    public Double getDano() {
        return dano;
    }

    public void setDano(Double dano) {
        this.dano = dano;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public Integer getIdPersonagem() {
        return idPersonagem;
    }

    public void setIdPersonagem(Integer idPersonagem) {
        this.idPersonagem = idPersonagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public RacaType getRaca() {
        return raca;
    }

    protected void setRaca(RacaType raca) {
        this.raca = raca;
    }

    //    protected void ganharItem(Item item){
//        this.inventario.adicionar(item);
//    }
//
//    protected void perderItem(Item item){
//        this.inventario.remover(item);
//    }
//
//    protected boolean podeSofrerDano(){
//        return this.vida > 0;
//    }
//
////    protected void aumentarXp(){
////        this.experiencia = experiencia + this.qtdExperienciaPorAtaque;
////    }
//
//
//    protected double calcularDano(){
//        return this.qtdDano;
//    }
//
//    protected void diminuirVida(){
//        if(this.podeSofrerDano()){
//            //comparacao ? verdadeiro : falso;
//            this.vida -= this.vida >= this.calcularDano() ? this.calcularDano(): this.vida;
//
//            if (this.vida == 0.0){
//                this.status = Status.MORTO;
//            }else{
//                //this.status = Status.SOFREU_DANO;
//            }
//        }
//    }
//
//    protected abstract String imprimirResultado();

}
