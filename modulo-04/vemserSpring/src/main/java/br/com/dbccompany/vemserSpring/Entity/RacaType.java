package br.com.dbccompany.vemserSpring.Entity;

public enum RacaType {
    ELFO,
    ELFO_VERDE,
    ELFO_NOTURNO,
    ELFO_DA_LUZ,
    DWARF,
    DWARF_BARBA_LONGA
}
