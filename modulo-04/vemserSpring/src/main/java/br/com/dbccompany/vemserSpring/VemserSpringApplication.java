package br.com.dbccompany.vemserSpring;

import br.com.dbccompany.vemserSpring.Entity.*;
import br.com.dbccompany.vemserSpring.Service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VemserSpringApplication {

	@Autowired
	ElfoService eService;

	@Autowired
	DwarfService dService;

	public static void main(String[] args) {
		SpringApplication.run(VemserSpringApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
//		return args -> {
//			Elfo elfo = new Elfo();
//			elfo.setNome("Legolas");
//			eService.salvar(elfo);
//
//			Dwarf dwarf = new Dwarf();
//			dwarf.setNome("Gimli");
//			dService.salvar(dwarf);
//		};
//	}



}
