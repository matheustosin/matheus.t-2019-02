package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.AgenciasDto;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasDAO extends AbstractDAO<Agencias>{

	public Agencias parseFrom(AgenciasDto dto) {
		Agencias agencias = null;
		if(dto.getIdAgencia() != null) {
			agencias = buscar(dto.getIdAgencia());
		} else {
			agencias = new Agencias();
		}
		agencias.setNome(dto.getNome());

		return agencias;
	}
	
	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}

}
