package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDto;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosDAO extends AbstractDAO<Bairros>{
	CidadesDAO dao = new CidadesDAO();
	
	public Bairros parseFrom(BairrosDto dto) {
		Bairros bairros = null;
		if(dto.getIdBairro() != null) {
			bairros = buscar(dto.getIdBairro());
		} else {
			bairros = new Bairros();
		}
		bairros.setNome(dto.getNome());
		bairros.setCidade(dao.parseFrom(dto.getCidades()));
		return bairros;
	}
	@Override
	protected Class<Bairros> getEntityClass() {
		return Bairros.class;
	}

}
