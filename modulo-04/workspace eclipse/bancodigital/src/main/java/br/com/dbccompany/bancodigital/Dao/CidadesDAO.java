package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CidadesDto;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesDAO extends AbstractDAO<Cidades>{
	EstadosDAO dao = new EstadosDAO();
	
	public Cidades parseFrom(CidadesDto dto) {
		Cidades cidades = null;
		if ( dto.getIdCidade() != null ) {
			cidades = buscar(dto.getIdCidade());
		} else {
			cidades = new Cidades();
		}
		cidades.setNome(dto.getNome());
		cidades.setEstado(dao.parseFrom(dto.getEstados()));
		return cidades;
	}
	@Override
	protected Class<Cidades> getEntityClass() {
		return Cidades.class;
	}

}
