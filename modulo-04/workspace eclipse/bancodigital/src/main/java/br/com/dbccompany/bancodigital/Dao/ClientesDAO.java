package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.ClientesDto;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesDAO extends AbstractDAO<Clientes>{

	public Clientes parseFrom(ClientesDto dto) {
		Clientes clientes = null;
		if(dto.getIdCliente() != null) {
			clientes = buscar(dto.getIdCliente());
		} else {
			clientes = new Clientes();
		}
		clientes.setNome(dto.getNome());
		return clientes;
	}
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}

}
