package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CorrentistasDto;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas>{

	public Correntistas parseFrom(CorrentistasDto dto) {
		Correntistas correntistas = null;
		if(dto.getIdCorrentista() != null) {
			correntistas = buscar(dto.getIdCorrentista());
		} else {
			correntistas = new Correntistas();
		}
		correntistas.setTipoConta(dto.getTipoConta());
		correntistas.setSaldo(dto.getSaldo());
		return correntistas;
	}
	
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}

}
