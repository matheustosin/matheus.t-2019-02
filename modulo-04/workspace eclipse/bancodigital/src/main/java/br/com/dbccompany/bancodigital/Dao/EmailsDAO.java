package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EmailsDto;
import br.com.dbccompany.bancodigital.Entity.Emails;

public class EmailsDAO extends AbstractDAO<Emails>{

	public Emails parseFrom(EmailsDto dto) {
		Emails emails = null;
		if(dto.getIdEmail() != null) {
			emails = buscar(dto.getIdEmail());
		} else {
			emails = new Emails();
		}
		emails.setEmail(dto.getEmail());
		return emails;
	}
	
	@Override
	protected Class<Emails> getEntityClass() {
		return Emails.class;
	}

}
