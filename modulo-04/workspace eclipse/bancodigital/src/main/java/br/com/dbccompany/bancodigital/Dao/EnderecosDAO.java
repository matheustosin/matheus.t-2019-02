package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EnderecosDto;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos>{
	BairrosDAO dao = new BairrosDAO();
	
	public Enderecos parseFrom(EnderecosDto dto) {
		Enderecos enderecos = null;
		if(dto.getIdEndereco() != null) {
			enderecos = buscar(dto.getIdEndereco());
		} else {
			enderecos = new Enderecos();
		}
		enderecos.setLogradouro(dto.getLogradouro());
		enderecos.setNumero(dto.getNumero());
		enderecos.setBairro(dao.parseFrom(dto.getBairros()));
		return enderecos;
	}
	@Override
	protected Class<Enderecos> getEntityClass() {
		return Enderecos.class;
	}

}
