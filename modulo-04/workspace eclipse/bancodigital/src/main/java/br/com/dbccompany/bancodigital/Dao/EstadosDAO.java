package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EstadosDto;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosDAO extends AbstractDAO<Estados>{
	PaisesDAO dao = new PaisesDAO();

	public Estados parseFrom(EstadosDto dto) {
		Estados estados = null;
		if( dto.getIdEstado() != null ) {
			estados = buscar(dto.getIdEstado());
		} else {
			estados = new Estados();
		}
		estados.setNome(dto.getNome());
		estados.setPais(dao.parseFrom(dto.getPaises()));
		return estados;
	}
	
	@Override
	protected Class<Estados> getEntityClass() {
		return Estados.class;
	}

}
