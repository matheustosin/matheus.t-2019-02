package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.TelefonesDto;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesDAO extends AbstractDAO<Telefones>{

	public Telefones parseFrom(TelefonesDto dto) {
		Telefones telefones = null;
		if(dto.getIdTelefone() != null) {
			telefones = buscar(dto.getIdTelefone());
		} else {
			telefones = new Telefones();
		}
		telefones.setNumero(dto.getNumero());
		return telefones;
	}
	@Override
	protected Class<Telefones> getEntityClass() {
		return Telefones.class;
	}

}
