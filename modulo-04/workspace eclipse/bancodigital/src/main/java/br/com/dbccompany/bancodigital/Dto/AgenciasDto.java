package br.com.dbccompany.bancodigital.Dto;

public class AgenciasDto {

	private Integer idAgencia;
	private String nome;
	
	private EnderecosDto enderecos;

	public Integer getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(Integer idAgencia) {
		this.idAgencia = idAgencia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnderecosDto getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(EnderecosDto enderecos) {
		this.enderecos = enderecos;
	}
	
	
}
