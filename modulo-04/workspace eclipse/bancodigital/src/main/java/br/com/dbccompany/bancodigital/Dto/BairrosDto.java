package br.com.dbccompany.bancodigital.Dto;

public class BairrosDto {

	private Integer idBairro;
	private String nome;
	
	private CidadesDto cidades;

	public Integer getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(Integer idBairro) {
		this.idBairro = idBairro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public CidadesDto getCidades() {
		return cidades;
	}

	public void setCidades(CidadesDto cidades) {
		this.cidades = cidades;
	}
	
	
}
