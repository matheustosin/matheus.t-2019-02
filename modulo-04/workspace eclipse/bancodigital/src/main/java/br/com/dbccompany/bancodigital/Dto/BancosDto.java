package br.com.dbccompany.bancodigital.Dto;

public class BancosDto {

	private Integer idBanco;
	private String nome;
	public Integer getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
