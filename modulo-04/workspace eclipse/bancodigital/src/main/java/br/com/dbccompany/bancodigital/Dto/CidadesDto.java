package br.com.dbccompany.bancodigital.Dto;

public class CidadesDto {

	private Integer idCidade;
	private String nome;
	
	private EstadosDto estados;

	public Integer getIdCidade() {
		return idCidade;
	}

	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadosDto getEstados() {
		return estados;
	}

	public void setEstados(EstadosDto estados) {
		this.estados = estados;
	}
	
	
}
