package br.com.dbccompany.bancodigital.Dto;

public class ClientesDto {

	private Integer idCliente;
	private String nome;
	
	private EnderecosDto enderecos;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnderecosDto getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(EnderecosDto enderecos) {
		this.enderecos = enderecos;
	}

	
	
	
}
