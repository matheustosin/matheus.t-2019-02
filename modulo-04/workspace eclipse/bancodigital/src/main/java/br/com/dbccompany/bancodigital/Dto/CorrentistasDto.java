package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.TipoContas;

public class CorrentistasDto {

	private Integer idCorrentista;
	private TipoContas tipoConta;
	private Double saldo = 0.0;
	
	public Integer getIdCorrentista() {
		return idCorrentista;
	}
	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}
	public TipoContas getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(TipoContas tipoConta) {
		this.tipoConta = tipoConta;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public boolean sacar(Double x) {
		if(x <= saldo) {
			this.saldo -= x;
			return true;
		}
		return false;
	}
	
	public boolean depositar(Double x) {
		if(x > 0) {
			this.saldo += x;
			return true;
		}
		return false;
	}
	
	public boolean transferir(Double dinheiro, CorrentistasDto destino) {
		if(dinheiro > 0 && destino != null) {
			boolean valido = this.sacar(dinheiro);
			if(valido) {
				destino.depositar(dinheiro);
				return true;
			}
		}
		return false;
	}
	
	public boolean solicitarEmprestimo(Double valor) {
		if(this.saldo == 0) {
			this.saldo += valor;
			return true;
		}
		return false;
	}
}
