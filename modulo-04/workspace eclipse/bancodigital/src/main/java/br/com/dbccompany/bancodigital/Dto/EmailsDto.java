package br.com.dbccompany.bancodigital.Dto;

public class EmailsDto {

	private Integer idEmail;
	private String email;
	
	private ClientesDto clientes;

	public Integer getIdEmail() {
		return idEmail;
	}

	public void setIdEmail(Integer idEmail) {
		this.idEmail = idEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ClientesDto getClientes() {
		return clientes;
	}

	public void setClientes(ClientesDto clientes) {
		this.clientes = clientes;
	}
	
	
}
