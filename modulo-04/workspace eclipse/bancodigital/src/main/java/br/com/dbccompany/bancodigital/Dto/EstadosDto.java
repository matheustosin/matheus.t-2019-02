package br.com.dbccompany.bancodigital.Dto;

public class EstadosDto {

	private Integer idEstado;
	private String nome;
	
	private PaisesDto paises;

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PaisesDto getPaises() {
		return paises;
	}

	public void setPaises(PaisesDto paises) {
		this.paises = paises;
	}
	
	
}
