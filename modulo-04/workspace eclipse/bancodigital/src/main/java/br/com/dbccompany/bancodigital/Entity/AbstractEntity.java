package br.com.dbccompany.bancodigital.Entity;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable{

	public abstract Integer getId();
}
