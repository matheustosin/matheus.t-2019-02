package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity

public class Agencias extends AbstractEntity{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(  name = "ID_AGENCIA")
	@SequenceGenerator( allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ" )
	@GeneratedValue( generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Column( name = "codigo", length = 100, nullable = false )
	private Integer codigo;
	
	@Column( name = "nome", length = 100, nullable = false )
	private String nome;
	
	@ManyToMany( mappedBy = "agencias", cascade = CascadeType.ALL )
	private List<Correntistas> correntistas = new ArrayList<>();
	
	@ManyToOne
	@JoinColumn( name = "fk_id_banco" )
	private Bancos banco;
	
	@ManyToOne
    @JoinColumn(name = "fk_id_endereco" )  
	private Enderecos endereco;

	public Integer getIdAgencia() {
		return id;
	}

	public void setIdAgencia(Integer idAgencia) {
		this.id = idAgencia;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Bancos getBanco() {
		return banco;
	}

	public void setBanco(Bancos banco) {
		this.banco = banco;
	}

	public Enderecos getEndereco() {
		return endereco;
	}

	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public List<Correntistas> getCorrentistas() {
		return correntistas;
	}

	public void setCorrentistas(List<Correntistas> correntistas) {
		this.correntistas = correntistas;
	}
	
	
	
	
}
