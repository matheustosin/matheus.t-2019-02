package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity

public class Cidades extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(  name = "ID_CIDADE")
	@SequenceGenerator( allocationSize = 1, name = "CIDADES_SEQ", sequenceName = "CIDADES_SEQ" )
	@GeneratedValue( generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Column( name = "nome", length = 100, nullable = false )
	private String nome;
	
	@OneToMany ( mappedBy = "cidade", cascade = CascadeType.ALL )
	private List<Bairros> bairros = new ArrayList<Bairros>();
	
	@ManyToOne
	@JoinColumn( name = "fk_id_estado" )
	private Estados estado;

	public Integer getIdCidade() {
		return id;
	}

	public void setIdCidade(Integer idCidade) {
		this.id = idCidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estados getEstado() {
		return estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	public List<Bairros> getBairros() {
		return bairros;
	}

	public void setBairros(List<Bairros> bairros) {
		this.bairros = bairros;
	}

	@Override
	public Integer getId() {
		return id;
	}


	
	
	
}
