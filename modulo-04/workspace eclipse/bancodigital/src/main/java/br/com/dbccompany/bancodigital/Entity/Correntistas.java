package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
public class Correntistas extends AbstractEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(  name = "ID_CORENTISTA")
	@GeneratedValue( generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@Column(name = "saldo")
	private Double saldo;
	
	@Column(name = "razaoSocial", length = 100)
	private String razaoSocial;
	
	@Column( name = "cnpj", length = 100)
	private String cnpj;
	
	@Enumerated(EnumType.STRING)
	private TipoContas tipoConta;
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable( name = "agencias_x_correntistas", 
					joinColumns = { @JoinColumn( name = "fk_id_correntista" )},
					inverseJoinColumns = { @JoinColumn( name = "fk_id_agencia" ) })
	private List<Agencias> agencias = new ArrayList<Agencias>();
	
	@ManyToMany( mappedBy = "correntistas" )
	private List<Clientes> clientes = new ArrayList<Clientes>();
	
	public Integer getIdCorrentista() {
		return id;
	}
	public void setIdCorrentista(Integer idCorrentista) {
		this.id = idCorrentista;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public TipoContas getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(TipoContas tipoConta) {
		this.tipoConta = tipoConta;
	}
	@Override
	public Integer getId() {
		return id;
	}
	
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public List<Clientes> getClientes() {
		return clientes;
	}
	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}
	public List<Agencias> getAgencias() {
		return agencias;
	}
	
	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
	

	
}
