package br.com.dbccompany.bancodigital.Entity;

public enum EstadoCivil {
	Solteiro, Casado, Divorciado, Viuvo;
}
