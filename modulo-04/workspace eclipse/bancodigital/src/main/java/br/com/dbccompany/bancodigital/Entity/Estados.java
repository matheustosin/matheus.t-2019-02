package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity

public class Estados extends AbstractEntity{

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "ESTADOS_SEQ", sequenceName = "ESTADOS_SEQ" )
	@GeneratedValue( generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_ESTADO" )
	private Integer id;
	
	@Column( name = "nome", length = 100, nullable = false )
	private String nome;

	@OneToMany( mappedBy = "estado", cascade = CascadeType.ALL )
	private List<Cidades> cidades = new ArrayList<Cidades>();
	
	@ManyToOne
	@JoinColumn( name = "fk_id_pais" )
	private Paises pais;

	public Integer getIdEstado() {
		return id;
	}

	public void setIdEstado(Integer idEstado) {
		this.id = idEstado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Paises getPais() {
		return pais;
	}

	public void setPais(Paises pais) {
		this.pais = pais;
	}

	public List<Cidades> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidades> cidades) {
		this.cidades = cidades;
	}

	@Override
	public Integer getId() {
		return id;
	}
	
	
}
