package br.com.dbccompany.bancodigital;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;

import br.com.dbccompany.bancodigital.Dto.CorrentistasDto;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.EstadoCivil;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.Entity.TipoContas;
import br.com.dbccompany.bancodigital.Sevices.AgenciasService;
import br.com.dbccompany.bancodigital.Sevices.BairrosService;
import br.com.dbccompany.bancodigital.Sevices.BancosService;
import br.com.dbccompany.bancodigital.Sevices.CidadesService;
import br.com.dbccompany.bancodigital.Sevices.ClientesService;
import br.com.dbccompany.bancodigital.Sevices.CorrentistasService;
import br.com.dbccompany.bancodigital.Sevices.EnderecosService;
import br.com.dbccompany.bancodigital.Sevices.EstadosService;
import br.com.dbccompany.bancodigital.Sevices.PaisesService;

public class Main {

	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	public static void main(String[] args) {
		
		Paises pais = new Paises();
		PaisesService paisesService = new PaisesService();
		pais.setNome("Brasil");
		paisesService.salvarPaises(pais);
		
		Paises pais2 = new Paises();
		pais2.setNome("EUA");
		paisesService.salvarPaises(pais2);
		
		Estados estado = new Estados();
		EstadosService estadosService = new EstadosService();
		estado.setNome("Paran�");
		estado.setPais(pais);
		estadosService.salvarEstados(estado);
		
		Estados estado2 = new Estados();
		estado2.setNome("California");
		estado2.setPais(pais2);
		estadosService.salvarEstados(estado2);
		
		Cidades cidade = new Cidades();
		CidadesService cidadesService = new CidadesService();
		cidade.setNome("Curitiba");
		cidade.setEstado(estado);
		cidadesService.salvarCidades(cidade);
		
		Bairros bairro = new Bairros();
		BairrosService bairrosService = new BairrosService();
		bairro.setNome("Bairro do Banco");
		bairro.setCidade(cidade);
		bairrosService.salvarBairros(bairro);
		
		Bancos banco = new Bancos();
		BancosService bancosService = new BancosService();
		banco.setNome("Bradesco");
		banco.setCodigo(100);
		bancosService.salvarBancos(banco);
		
		Bancos banco1 = new Bancos();
		banco1.setNome("Itau");
		banco1.setCodigo(200);
		bancosService.salvarBancos(banco1);
		
		Enderecos endereco = new Enderecos();
		EnderecosService enderecosService = new EnderecosService();
		endereco.setLogradouro("Rua do Bradesco");
		endereco.setNumero(31);
		enderecosService.salvarEnderecos(endereco);
		
		Enderecos endereco1 = new Enderecos();
		endereco1.setLogradouro("Rua do Itau");
		endereco1.setNumero(56);
		enderecosService.salvarEnderecos(endereco1);
		
		Enderecos endereco2 = new Enderecos();
		endereco2.setLogradouro("Rua do Cliente");
		endereco2.setNumero(96);
		enderecosService.salvarEnderecos(endereco2);
		
		Enderecos endereco3 = new Enderecos();
		endereco3.setLogradouro("Rua do Outro Cliente");
		endereco3.setNumero(78);
		enderecosService.salvarEnderecos(endereco3);
		
		Agencias agencia = new Agencias();
		AgenciasService agenciasService = new AgenciasService();
		agencia.setNome("Agencia Bradesco");
		agencia.setCodigo(576);
		agencia.setEndereco(endereco);
		agencia.setBanco(banco1);
		
		Agencias agencia1 = new Agencias();
		agencia1.setNome("Agencia Itau");
		agencia1.setCodigo(485);
		agencia1.setEndereco(endereco1);
		agencia1.setBanco(banco1);
		agenciasService.salvarAgencias(agencia1);
		
		Clientes cliente = new Clientes();
		ClientesService clientesService = new ClientesService();
		cliente.setCpf("012.256.314-01");
		cliente.setNome("Joao");
		cliente.setRg("1152635897");
		cliente.setDataNascimento("01/08/1999");
		cliente.setEndereco(endereco2);
		cliente.setEstadoCivil(EstadoCivil.Solteiro);
		Correntistas correntistas1 = new Correntistas();
		List<Correntistas> listCorrCli = new ArrayList<Correntistas>();
		listCorrCli.add(correntistas1);
		cliente.setCorrentistas(listCorrCli);
		clientesService.salvarClientes(cliente);
		
		Clientes cliente1 = new Clientes();
		cliente1.setCpf("985.125.125-11");
		cliente1.setNome("Maria");
		cliente1.setRg("2235689471");
		cliente1.setDataNascimento("30/10/2000");
		cliente1.setEndereco(endereco3);
		cliente1.setEstadoCivil(EstadoCivil.Solteiro);
		Correntistas correntista = new Correntistas();
		List<Correntistas> listCorrCli1 = new ArrayList<Correntistas>();
		listCorrCli1.add(correntista);
		cliente1.setCorrentistas(listCorrCli1);
		clientesService.salvarClientes(cliente1);
		
		
		CorrentistasService correntistasService = new CorrentistasService();
		correntista.setTipoConta(TipoContas.PF);
		correntistasService.salvarCorrentistas(correntista);
		
		
		correntistas1.setTipoConta(TipoContas.Investimento);
		correntistasService.salvarCorrentistas(correntistas1);	
		
		List<Clientes> listCli = new ArrayList<>();
		listCli.add(cliente);
		correntista.setClientes(listCli);
		
		List<Agencias> listAgencia = new ArrayList<>();
		listAgencia.add(agencia);
		correntista.setAgencias(listAgencia);
		
		List<Correntistas> lstCorr = new ArrayList<Correntistas>();
		lstCorr.add(correntista);
		agencia.setCorrentistas(lstCorr);
		
		agenciasService.salvarAgencias(agencia);

		
		
		
		CorrentistasDto conta1 = new CorrentistasDto();
		CorrentistasDto conta2 = new CorrentistasDto();
		conta1.setTipoConta(TipoContas.PF);
		conta1.depositar(100.0);
		conta2.setTipoConta(TipoContas.PF);
		conta2.depositar(100.0);
//		correntistasService11.salvarCorrentistas(conta1);
//		correntistasService11.salvarCorrentistas(conta2);
		
		System.out.println("Conta 01: " + conta1.getSaldo());
		System.out.println("Conta 02: " + conta2.getSaldo());

		conta1.transferir(100.0, conta2);
//		correntistasService11.salvarCorrentistas(conta1);
//		correntistasService11.salvarCorrentistas(conta2);
		
		System.out.println("Conta 01: " + conta1.getSaldo());
		System.out.println("Conta 02: " + conta2.getSaldo());
		
		conta1.solicitarEmprestimo(100.00);
		correntistasService.salvarCorrentistas(conta1);
		correntistasService.salvarCorrentistas(conta2);
		System.out.println("Conta 01: " + conta1.getSaldo());
		System.out.println("Conta 02: " + conta2.getSaldo());
		
		
		
	
		
		System.exit(0);
	
	}
	
//	public static void main(String[] args) {
//		
//		Session session = null;
//		Transaction transaction = null;
//		try {
//			session = HibernateUtil.getSession();
//			transaction = session.beginTransaction();
//			
//			Paises paises = new Paises();
//			paises.setNome("Brasil");
//			
//			session.save(paises);
//			
//			//session.createQuery("select * from paises;").executeUpdate();
//					
//			Criteria criteria = session.createCriteria(Paises.class);
//			criteria.createAlias("nome", "nome_paises");
//			criteria.add(
//					Restrictions.isNotNull("nome")
//			);
//			
//			List<Paises> lstPaises = criteria.list();
//			
//			transaction.commit();
//		} catch (Exception e) {
//			if (transaction != null) {
//				transaction.rollback();
//			}
//			LOG.log(Level.SEVERE, e.getMessage(), e);
//			System.exit(1);
//		} finally {
//			if (session != null) {
//				session.close();
//			}
//		}
//		System.exit(0);
//	}
}
