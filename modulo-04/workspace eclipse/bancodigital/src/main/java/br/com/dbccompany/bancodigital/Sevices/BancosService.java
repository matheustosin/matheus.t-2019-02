package br.com.dbccompany.bancodigital.Sevices;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BancosDAO;
import br.com.dbccompany.bancodigital.Dto.BancosDto;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class BancosService {

	private static final BancosDAO BANCOS_DAO = new BancosDAO();
	private static final Logger LOG = Logger.getLogger(BancosService.class.getName());
	
	public void salvarBancos(Bancos b) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			BANCOS_DAO.criar(b);
			if(started) {
				transaction.commit();
			}
		} catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarBancos(BancosDto bDto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bancos bancos = BANCOS_DAO.parseFrom(bDto);
		
		try {
			Bancos bancosRes = BANCOS_DAO.buscar(1);
			if(bancosRes == null) {
				BANCOS_DAO.criar(bancos);
			} else {
				bancos.setIdBanco(bancosRes.getId());
				BANCOS_DAO.atualizar(bancosRes);
			}
			if(started) {
				transaction.commit();
			}
		} catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
