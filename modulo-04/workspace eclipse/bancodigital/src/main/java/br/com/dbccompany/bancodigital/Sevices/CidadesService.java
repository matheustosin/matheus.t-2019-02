package br.com.dbccompany.bancodigital.Sevices;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CidadesDAO;
import br.com.dbccompany.bancodigital.Dto.CidadesDto;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class CidadesService {

	
	private static final CidadesDAO CIDADES_DAO = new CidadesDAO();
	private static final Logger LOG = Logger.getLogger(CidadesService.class.getName());
	
	public void salvarCidades(Cidades c) {
		boolean started  = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			CIDADES_DAO.criar(c);
			if (started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarCidades(CidadesDto cDto) {
		boolean started  = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Cidades cidades = CIDADES_DAO.parseFrom(cDto);
		
		try {
			Cidades cidadesRes = CIDADES_DAO.buscar(1);
			if(cidadesRes == null) {
				CIDADES_DAO.criar(cidades);
			} else {
				cidades.setIdCidade(cidadesRes.getId());
				CIDADES_DAO.atualizar(cidades);
			}
			if (started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
