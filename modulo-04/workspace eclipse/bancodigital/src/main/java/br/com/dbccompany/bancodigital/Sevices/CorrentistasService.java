package br.com.dbccompany.bancodigital.Sevices;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDto;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class CorrentistasService {

	
	private static final CorrentistasDAO CORRENTISTAS_DAO = new CorrentistasDAO();
	private static final Logger LOG = Logger.getLogger(CorrentistasService.class.getName());
	
	public void salvarCorrentistas(Correntistas c) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			CORRENTISTAS_DAO.criar(c);
			if(started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarCorrentistas(CorrentistasDto cDto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Correntistas correntistas = CORRENTISTAS_DAO.parseFrom(cDto);
		
		try {
			Correntistas correntistasRes = CORRENTISTAS_DAO.buscar(1);
			if(correntistasRes == null) {
				CORRENTISTAS_DAO.criar(correntistas);
			} else {
				correntistas.setIdCorrentista(correntistasRes.getId());
				CORRENTISTAS_DAO.atualizar(correntistas);
			}
			if(started) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
