package br.com.dbccompany.bancodigital.Sevices;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EmailsDAO;
import br.com.dbccompany.bancodigital.Dto.EmailsDto;
import br.com.dbccompany.bancodigital.Entity.Emails;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class EmailsService {

	private static final EmailsDAO EMAILS_DAO = new EmailsDAO();
	private static final Logger LOG = Logger.getLogger(EmailsService.class.getName());
	
	public void salvarEmails(Emails em) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			EMAILS_DAO.criar(em);
			if(started ) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public void salvarEmails(EmailsDto emDto) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Emails emails = EMAILS_DAO.parseFrom(emDto);
		
		try {
			Emails emailsRes = EMAILS_DAO.buscar(1);
			if(emailsRes == null) {
				EMAILS_DAO.criar(emails);
			} else {
				emails.setIdEmail(emailsRes.getId());
				EMAILS_DAO.atualizar(emails);
			}
			if(started ) {
				transaction.commit();
			}
		} catch (Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
